﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace DotNetShop.DAL.Model
{
    public partial class Order
    {
        public Order()
        {
            Invoices = new HashSet<Invoice>();
            OrderElems = new HashSet<OrderElem>();
        }
        public int Id { get; set; }
        public string UserId { get; set; }
        public virtual AppUser User { get; set; }

        [Required(ErrorMessage = "Wprowadzić imię")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Wprowadź nazwisko")]
        [StringLength(150)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Nie wprowadzono adresu")]
        [StringLength(150)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Wprowadź kod pocztowy")]
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required(ErrorMessage = "Wprowadź miasto")]
        [StringLength(50)]
        public string City { get; set; }

        [Required(ErrorMessage = "Musisz wprowadzić numer telefonu")]
        [StringLength(20)]
        [RegularExpression(@"(\+\d{2})*[\d\s-]+",
            ErrorMessage = "Błędny format numeru telefonu.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Wprowadź swój adres e-mail.")]
        [EmailAddress(ErrorMessage = "Błędny format adresu e-mail.")]
        public string Email { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime DatePurchased { get; set; }
        public DateTime? DateShipment { get; set; }
        public DateTime? DateFinished { get; set; }
        public string Comment { get; set; }
        [Column(TypeName="decimal(10,2")]
        public decimal TotalPrice { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public bool IsPaid { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<OrderElem> OrderElems { get; set; }
    }
    public enum OrderStatus
    {
        [Display(Name="Nowe")]
        New,
        [Display(Name="Anulowane")]
        Canceled,
        [Display(Name="Zakończone")]
        Finished
    }
    public enum PaymentMethod
    {
        [Display(Name = "Kurier (Płatność z góry - 15,00zł)")]
        PaymentInAdvance,
        [Display(Name = "Kurier (Płatność pobranie - 25,00zł)")]
        PaymentOnDelivery
    }

}
