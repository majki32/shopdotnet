﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace DotNetShop.DAL.Model
{
    [JsonObject(IsReference = true)]
    public partial class Product
    {
        public Product()
        {
            Manufacturers = new HashSet<Manufacturer>();
            OrderElems = new HashSet<OrderElem>();
            ProductsGalleries = new HashSet<ProductsGallery>();
            ProductsOpinions = new HashSet<ProductsOpinion>();
            ProductsSpecValues = new HashSet<ProductsSpecValue>();
        }

        public int Id { get; set; }
        public int? CategoryId { get; set; }
        public int ManufacturerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        //public bool? IsOnSale { get; set; }
        //public decimal? SaleValue { get; set; }
        //public TimeSpan? SpecialOfferTime { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string ProductUniqueId { get; set; }
        public bool IsHidden { get; set; }
        public DateTime ProductTimeCreated { get; set; }
        public DateTime? ProductTimeModified { get; set; }

        public virtual Category Category { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Warehouse Warehouse { get; set; }
        public virtual ICollection<HiddenProducts> HiddenProduct { get; set; }
        public virtual ICollection<Manufacturer> Manufacturers { get; set; }
        public virtual ICollection<OrderElem> OrderElems { get; set; }
        public virtual ICollection<ProductsGallery> ProductsGalleries { get; set; }
        public virtual ICollection<ProductsOpinion> ProductsOpinions { get; set; }
        public virtual ICollection<ProductsSpecValue> ProductsSpecValues { get; set; }
    }
}
