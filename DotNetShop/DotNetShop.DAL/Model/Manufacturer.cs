﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DotNetShop.DAL.Model
{
    public partial class Manufacturer
    {
        public Manufacturer()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ManufacturerLogoPath { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
