﻿using DotNetShop.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DotNetShop.DAL.Model
{
    public partial class MicomDBContext : IdentityDbContext<AppUser>
    {
        public MicomDBContext()
        {
        }

        public MicomDBContext(DbContextOptions<MicomDBContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<Manufacturer> Manufacturers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderElem> OrderElems { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductsGallery> ProductsGalleries { get; set; }
        public virtual DbSet<ProductsOpinion> ProductsOpinions { get; set; }
        public virtual DbSet<ProductsSpecValue> ProductsSpecValues { get; set; }
        public virtual DbSet<ProductsSpecification> ProductsSpecifications { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<UserData> UserDatas { get; set; }
        public virtual DbSet<HiddenProducts> HiddenProducts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=MicomDB;Trusted_Connection=True;MultipleActiveResultSets=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasAnnotation("Relational:Collation", "Polish_CI_AS");

            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasIndex(e => e.ParentCategoryId, "Relationship_11_FK");

                entity.Property(e => e.CategoryIconName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ParentCategory)
                    .WithMany(p => p.InverseParentCategory)
                    .HasForeignKey(d => d.ParentCategoryId)
                    .HasConstraintName("FK_CATEGORI_RELATIONS_CATEGORI");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasIndex(e => e.OrderId, "OrdersToInvoices_FK");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Vat).HasColumnName("VAT");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_INVOICES_ORDERSTOI_ORDERS");
            });

            modelBuilder.Entity<Manufacturer>(entity =>
            {

                entity.Property(e => e.ManufacturerLogoPath)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.DateFinished).HasColumnType("datetime");

                entity.Property(e => e.DatePurchased).HasColumnType("datetime");

                entity.Property(e => e.DateShipment).HasColumnType("datetime");

                entity.Property(e => e.OrderStatus)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<OrderElem>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ProductId, e.OrderId })
                    .HasName("PK_ORDERELEM");

                entity.ToTable("OrderElem");

                entity.HasIndex(e => e.ProductId, "Relationship_10_FK");

                entity.HasIndex(e => e.OrderId, "Relationship_12_FK");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderElems)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDERELE_RELATIONS_ORDERS");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.OrderElems)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORDERELE_RELATIONS_PRODUCTS");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasIndex(e => e.CategoryId, "ProductsToCategories_FK");

                entity.HasIndex(e => e.ManufacturerId, "ProductsToManufacturers2_FK");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_PRODUCTS_PRODUCTST_CATEGORI");

                entity.HasOne(d => d.Manufacturer)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ManufacturerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRODUCTS_PRODUCTST_MANUFACT");
            });

            modelBuilder.Entity<ProductsGallery>(entity =>
            {
                entity.ToTable("ProductsGallery");

                entity.HasIndex(e => e.ProductId, "ProductsToProductsGallery_FK");

                entity.Property(e => e.PhotoUrl)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductsGalleries)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_PRODUCTS_PRODUCTST_PRODUCTS");
            });

            modelBuilder.Entity<ProductsOpinion>(entity =>
            {
                entity.ToTable("ProductsOpinion");

                entity.HasIndex(e => e.ProductId, "Products_To_ProductsOpinion_FK");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Opinion)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductsOpinions)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION2");
            });

            modelBuilder.Entity<ProductsSpecValue>(entity =>
            {
                entity.ToTable("ProductsSpecValue");

                entity.HasIndex(e => e.ProductId, "Products_To_ProductsSpecification2_FK");

                entity.HasIndex(e => e.SpecificationId, "Products_To_ProductsSpecification_FK");

                entity.Property(e => e.SpecValue).HasMaxLength(50);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductsSpecValues)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION");

                entity.HasOne(d => d.Specification)
                    .WithMany(p => p.ProductsSpecValues)
                    .HasForeignKey(d => d.SpecificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPEC");
            });

            modelBuilder.Entity<ProductsSpecification>(entity =>
            {
                entity.ToTable("ProductsSpecification");

                entity.Property(e => e.SpecName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Warehouse>(entity =>
            {
                entity.ToTable("Warehouse");
            });
            modelBuilder.SeedDemoData();
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
