﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace DotNetShop.DAL.Model
{
    public partial class Invoice
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public string Number { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal Value { get; set; }
        public int Vat { get; set; }
        public string BankName { get; set; }

        public virtual Order Order { get; set; }
    }
}
