﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DotNetShop.DAL.Model
{
    public class HiddenProducts
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string HideReason { get; set; }
        public DateTime ProductHideDate { get; set; }

        [ForeignKey("ProductId")]
        public Product Product { get; set; }
    }
}
