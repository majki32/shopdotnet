﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DotNetShop.DAL.Model
{
    public class UserData
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public AppUser User { get; set; }
#nullable enable
        [Column(TypeName = "nvarchar(50)")]
        public string? CompanyName { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string? Nip { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        [PersonalData]
        public string? Address { get; set; }
        [PersonalData]
        [Column(TypeName = "varchar(10)")]
        public string? ZipCode { get; set; }
        [PersonalData]
        [Column(TypeName = "nvarchar(50)")]
        public string? City { get; set; }
#nullable disable
    }
}
