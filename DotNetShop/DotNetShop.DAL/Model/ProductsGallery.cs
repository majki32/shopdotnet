﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace DotNetShop.DAL.Model
{
    public partial class ProductsGallery
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string PhotoUrl { get; set; }

        public virtual Product Product { get; set; }
    }
}
