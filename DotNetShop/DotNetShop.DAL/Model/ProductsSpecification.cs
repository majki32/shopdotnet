﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace DotNetShop.DAL.Model
{
    [JsonObject(IsReference = true)]
    public partial class ProductsSpecification
    {
        public ProductsSpecification()
        {
            ProductsSpecValues = new HashSet<ProductsSpecValue>();
        }

        public int Id { get; set; }
        public string SpecName { get; set; }

        public virtual ICollection<ProductsSpecValue> ProductsSpecValues { get; set; }
    }
}
