﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DotNetShop.DAL.Model
{
    public partial class ProductsOpinion
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string Opinion { get; set; }
        public DateTime Date { get; set; }
        public short Rating { get; set; }
        public short? Helpful { get; set; }
        public short? NotHelpful { get; set; }
        public bool IsConfirmBuy { get; set; }

        public virtual Product Product { get; set; }
    }
}
