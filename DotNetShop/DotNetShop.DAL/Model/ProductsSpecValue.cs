﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace DotNetShop.DAL.Model
{
    [JsonObject(IsReference = true)]
    public partial class ProductsSpecValue
    {
        public int Id { get; set; }
        public string SpecValue { get; set; }
        public int ProductId { get; set; }
        public int SpecificationId { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductsSpecification Specification { get; set; }
    }
}
