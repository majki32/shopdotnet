﻿using DotNetShop.DAL.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DotNetShop.Extensions
{
    public static class DatabaseInitializer
    {
        public static void SeedIdentity(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }
        public static void SeedUsers(UserManager<AppUser> userManager)
        {
            AppUser user = new AppUser()
            {
                FirstName = "Admin",
                LastName = "Admin",
                Email = "admin@admin.com",
                UserName = "Admin"
            };
            IdentityResult result = userManager.CreateAsync(user, "123456q").Result;
            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(user, "Admin");
                using (var dbContext = new MicomDBContext())
                {
                    var userData = dbContext.UserDatas.FirstOrDefaultAsync(x => x.UserId == user.Id);

                    if (userData != null)
                    {
                        var data = new UserData()
                        {
                            UserId = user.Id
                        };
                        dbContext.UserDatas.Add(data);
                    }
                    dbContext.SaveChanges();
                }
            }
        }
        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            bool isRoleExist = roleManager.RoleExistsAsync("Admin").Result;
            if (!isRoleExist)
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
            isRoleExist = roleManager.RoleExistsAsync("User").Result;
            if (!isRoleExist)
            {
                var role = new IdentityRole();
                role.Name = "User";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
        }
        public static void SeedDemoData(this ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Author>().HasData(
            //    new Author
            //    {
            //        AuthorId = 1,
            //        FirstName = "William",
            //        LastName = "Shakespeare"
            //    }
            //);
            //modelBuilder.Entity<Book>().HasData(
            //    new Book { BookId = 1, AuthorId = 1, Title = "Hamlet" },
            //    new Book { BookId = 2, AuthorId = 1, Title = "King Lear" },
            //    new Book { BookId = 3, AuthorId = 1, Title = "Othello" }
            //);

            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, ParentCategoryId = null, Name = "Laptopy i komputery", CategoryIconName = "laptop" },
                new Category { Id = 2, ParentCategoryId = 1, Name = "Laptopy 2 w 1", CategoryIconName = "laptop" },
                new Category { Id = 3, ParentCategoryId = 1, Name = "Laptopy/Notebooki/Ultrabooki", CategoryIconName = "laptop" },
                new Category { Id = 4, ParentCategoryId = 3, Name = "Notebooki/Laptopy 17.3", CategoryIconName = "laptop" },
                new Category { Id = 5, ParentCategoryId = 3, Name = "Notebooki/Laptopy 15.6", CategoryIconName = "laptop" },
                new Category { Id = 6, ParentCategoryId = null, Name = "Podzespoły komputerowe", CategoryIconName = "memory" },
                new Category { Id = 7, ParentCategoryId = 6, Name = "Dyski twarde HDD i SSD", CategoryIconName = "memory" },
                new Category { Id = 8, ParentCategoryId = 7, Name = "Dyski HDD", CategoryIconName = "memory" },
                new Category { Id = 9, ParentCategoryId = 7, Name = "Dyski SSD", CategoryIconName = "memory" },
                new Category { Id = 10, ParentCategoryId = 6, Name = "Karty graficzne", CategoryIconName = "memory" },
                new Category { Id = 11, ParentCategoryId = 10, Name = "Karty graficzne NVIDIA", CategoryIconName = "memory" },
                new Category { Id = 12, ParentCategoryId = 10, Name = "Karty graficzne AMD", CategoryIconName = "memory" },
                new Category { Id = 13, ParentCategoryId = null, Name = "Smartfony", CategoryIconName = "phone_android" },
                new Category { Id = 14, ParentCategoryId = 13, Name = "Smartfony i telefony", CategoryIconName = "phone_android" },
                new Category { Id = 15, ParentCategoryId = 13, Name = "Akcesoria GSM", CategoryIconName = "phone_android" },
                new Category { Id = 16, ParentCategoryId = 15, Name = "Powerbanki", CategoryIconName = "phone_android" }
                );
            modelBuilder.Entity<Manufacturer>().HasData(
                new Manufacturer { Id = 1, Name = "Sony", ManufacturerLogoPath = "/img/Manufacturers/sony.png" },
                new Manufacturer { Id = 2, Name = "Gigabyte", ManufacturerLogoPath = "/img/Manufacturers/gigabyte.png" },
                new Manufacturer { Id = 3, Name = "Huawei", ManufacturerLogoPath = "/img/Manufacturers/huawei.png" },
                new Manufacturer { Id = 4, Name = "MSI", ManufacturerLogoPath = "/img/Manufacturers/msi.png" },
                new Manufacturer { Id = 5, Name = "Dell", ManufacturerLogoPath = "/img/Manufacturers/dell.png" },
                new Manufacturer { Id = 6, Name = "Asus", ManufacturerLogoPath = "/img/Manufacturers/asus.png" }
            );
            modelBuilder.Entity<ProductsSpecification>().HasData(
                new ProductsSpecification { Id = 1, SpecName = "Procesor" },
                new ProductsSpecification { Id = 2, SpecName = "Pamięć RAM" },
                new ProductsSpecification { Id = 3, SpecName = "Dysk SSD" },
                new ProductsSpecification { Id = 4, SpecName = "Karta graficzna" },
                new ProductsSpecification { Id = 5, SpecName = "Kolor" },
                new ProductsSpecification { Id = 6, SpecName = "System operacyjny" },
                new ProductsSpecification { Id = 7, SpecName = "Rozdzielczość ekranu" },
                new ProductsSpecification { Id = 8, SpecName = "Przekątna ekranu" },
                new ProductsSpecification { Id = 9, SpecName = "Dźwięk" },
                new ProductsSpecification { Id = 10, SpecName = "Pojemność" },
                new ProductsSpecification { Id = 11, SpecName = "Odczyt losowy" },
                new ProductsSpecification { Id = 12, SpecName = "Zapis losowy" },
                new ProductsSpecification { Id = 13, SpecName = "Niezawodność MTBF" },
                new ProductsSpecification { Id = 14, SpecName = "Interfejs" },
                new ProductsSpecification { Id = 15, SpecName = "Format" },
                new ProductsSpecification { Id = 16, SpecName = "Rodzaj pamięci" },
                new ProductsSpecification { Id = 17, SpecName = "Szyna pamięci" },
                new ProductsSpecification { Id = 18, SpecName = "Złącze zasilania" },
                new ProductsSpecification { Id = 19, SpecName = "Układ graficzny" },
                new ProductsSpecification { Id = 20, SpecName = "Pamięć" },
                new ProductsSpecification { Id = 21, SpecName = "Taktowanie rdzenia" },
                new ProductsSpecification { Id = 22, SpecName = "Rdzenie CUDA" }
            );
            modelBuilder.Entity<Product>().HasData(
                new Product { Id = 1, ManufacturerId = 2, CategoryId = 4, Description = "<p class='product-description-text'>Gotuj się do walki. Gamingowy laptop Gigabyte AORUS 17G wprowadzi Cię na pola wirtualnych bitew, oddając do dyspozycji arsenał, który poprowadzi Cię do niezliczonych zwycięstw. Wyposażony został w wyselekcjonowane, ultrawydajne komponenty - nowoczesny procesor oraz dedykowaną kartę graficzną. Z takim zapleczem technologicznym Twoi rywale mogą co najwyżej przygotowywać się do odwrotu.</p><img src='/img/Products/000-000-a1/1.jpeg' /><p class='product-description-text'>Wejdź do gry i poczuj smak zwycięstwa dzięki najnowszej platformie RTX z architekturą NVIDIA Ampere. Wyposażono ją w dedykowane rdzenie RT odpowiadającej za ray tracing oraz rdzenie Tensor na potrzeby przetwarzania SI. Wykorzystując technologię głębokiego, maszynowego uczenia się – DLSS (Deep Learning Super Sampling), procesor graficzny zwiększy częstotliwość generowania klatek przy zachowaniu pięknych, ostrych obrazów w grach.</p>'", IsHidden = false, Model = "17G XD-73EE345SH", Name = "Gigabyte AORUS 17G", ProductUniqueId = "000-000-a1", Price = 10499 },
                new Product { Id = 2, ManufacturerId = 3, CategoryId = 14, Description = "opis do produktu", IsHidden = false, Model = "Peppa-L21B Crush Green", Name = "Huawei P smart 2021", ProductUniqueId = "000-000-a2", Price = 699 }
            );
            modelBuilder.Entity<ProductsGallery>().HasData(
                new ProductsGallery { Id = 1, ProductId = 1, PhotoUrl = "/img/Products/000-000-a1/1.jpeg" },
                new ProductsGallery { Id = 2, ProductId = 1, PhotoUrl = "/img/Products/000-000-a1/2.jpeg" },
                new ProductsGallery { Id = 3, ProductId = 1, PhotoUrl = "/img/Products/000-000-a1/3.jpeg" },
                new ProductsGallery { Id = 4, ProductId = 1, PhotoUrl = "/img/Products/000-000-a1/4.jpeg" },
                new ProductsGallery { Id = 5, ProductId = 2, PhotoUrl = "/img/Products/000-000-a2/1.jpg" },
                new ProductsGallery { Id = 6, ProductId = 2, PhotoUrl = "/img/Products/000-000-a2/2.jpg" },
                new ProductsGallery { Id = 7, ProductId = 2, PhotoUrl = "/img/Products/000-000-a2/3.jpg" }
            );
            modelBuilder.Entity<Warehouse>().HasData(
                new Warehouse { Id = 1, ProductId = 1, Quantity = 20},
                new Warehouse { Id = 2, ProductId = 2, Quantity = 12 }
                );
            modelBuilder.Entity<ProductsSpecValue>().HasData(
                new ProductsSpecValue { Id = 1, ProductId = 1, SpecificationId = 1, SpecValue = "Intel Core i7-11800H" },
                new ProductsSpecValue { Id = 2, ProductId = 1, SpecificationId = 2, SpecValue = "32 GB" },
                new ProductsSpecValue { Id = 3, ProductId = 1, SpecificationId = 3, SpecValue = "512 GB" },
                new ProductsSpecValue { Id = 4, ProductId = 1, SpecificationId = 4, SpecValue = "NVIDIA GeForce RTX 3070" },
                new ProductsSpecValue { Id = 5, ProductId = 1, SpecificationId = 5, SpecValue = "Czarny" },
                new ProductsSpecValue { Id = 6, ProductId = 1, SpecificationId = 6, SpecValue = "Windows 10 Home" },
                new ProductsSpecValue { Id = 7, ProductId = 1, SpecificationId = 7, SpecValue = "1920 x 1080 (FullHD)" },
                new ProductsSpecValue { Id = 8, ProductId = 1, SpecificationId = 8, SpecValue = "17,3" },
                new ProductsSpecValue { Id = 9, ProductId = 1, SpecificationId = 9, SpecValue = "Wbudowane głośniki stereo" },
                new ProductsSpecValue { Id = 10, ProductId = 1, SpecificationId = 10, SpecValue = "512 GB" },
                new ProductsSpecValue { Id = 11, ProductId = 2, SpecificationId = 1, SpecValue = "Kirin 710A" },
                new ProductsSpecValue { Id = 12, ProductId = 2, SpecificationId = 2, SpecValue = "4 GB" },
                new ProductsSpecValue { Id = 13, ProductId = 2, SpecificationId = 3, SpecValue = "128 GB" },
                new ProductsSpecValue { Id = 14, ProductId = 2, SpecificationId = 4, SpecValue = "Mali-G51 MP4" },
                new ProductsSpecValue { Id = 15, ProductId = 2, SpecificationId = 5, SpecValue = "Zielony" },
                new ProductsSpecValue { Id = 16, ProductId = 2, SpecificationId = 6, SpecValue = "Android 10" },
                new ProductsSpecValue { Id = 17, ProductId = 2, SpecificationId = 7, SpecValue = "2400 x 1080" },
                new ProductsSpecValue { Id = 18, ProductId = 2, SpecificationId = 8, SpecValue = "6,67" },
                new ProductsSpecValue { Id = 19, ProductId = 2, SpecificationId = 9, SpecValue = "Wbudowane głośniki stereo" },
                new ProductsSpecValue { Id = 20, ProductId = 2, SpecificationId = 10, SpecValue = "128 GB" }
            );
        }
    }
}
