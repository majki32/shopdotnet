﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DotNetShop.DAL.Migrations
{
    public partial class AddingDemoData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsOnSale",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SaleValue",
                table: "Products");

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CategoryIconName", "Name", "ParentCategoryId" },
                values: new object[,]
                {
                    { 1, "laptop", "Laptopy i komputery", null },
                    { 6, "memory", "Podzespoły komputerowe", null },
                    { 13, "phone_android", "Smartfony", null }
                });

            migrationBuilder.InsertData(
                table: "Manufacturers",
                columns: new[] { "Id", "ManufacturerLogoPath", "Name", "ProductId" },
                values: new object[,]
                {
                    { 1, "/img/Manufacturers/sony.png", "Sony", null },
                    { 2, "/img/Manufacturers/gigabyte.png", "Gigabyte", null },
                    { 3, "/img/Manufacturers/huawei.png", "Huawei", null },
                    { 4, "/img/Manufacturers/msi.png", "MSI", null },
                    { 5, "/img/Manufacturers/dell.png", "Dell", null },
                    { 6, "/img/Manufacturers/asus.png", "Asus", null }
                });

            migrationBuilder.InsertData(
                table: "ProductsSpecification",
                columns: new[] { "Id", "SpecName" },
                values: new object[,]
                {
                    { 13, "Niezawodność MTBF" },
                    { 14, "Interfejs" },
                    { 15, "Format" },
                    { 16, "Rodzaj pamięci" },
                    { 20, "Pamięć" },
                    { 18, "Złącze zasilania" },
                    { 19, "Układ graficzny" },
                    { 12, "Zapis losowy" },
                    { 17, "Szyna pamięci" },
                    { 11, "Odczyt losowy" },
                    { 7, "Rozdzielczość ekranu" },
                    { 9, "Dźwięk" },
                    { 8, "Przekątna ekranu" },
                    { 21, "Taktowanie rdzenia" },
                    { 6, "System operacyjny" },
                    { 5, "Kolor" },
                    { 4, "Karta graficzna" },
                    { 3, "Dysk SSD" },
                    { 2, "Pamięć RAM" },
                    { 1, "Procesor" },
                    { 10, "Pojemność" },
                    { 22, "Rdzenie CUDA" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CategoryIconName", "Name", "ParentCategoryId" },
                values: new object[,]
                {
                    { 2, "laptop", "Laptopy 2 w 1", 1 },
                    { 3, "laptop", "Laptopy/Notebooki/Ultrabooki", 1 },
                    { 7, "memory", "Dyski twarde HDD i SSD", 6 },
                    { 10, "memory", "Karty graficzne", 6 },
                    { 14, "phone_android", "Smartfony i telefony", 13 },
                    { 15, "phone_android", "Akcesoria GSM", 13 }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CategoryIconName", "Name", "ParentCategoryId" },
                values: new object[,]
                {
                    { 4, "laptop", "Notebooki/Laptopy 17.3", 3 },
                    { 5, "laptop", "Notebooki/Laptopy 15.6", 3 },
                    { 8, "memory", "Dyski HDD", 7 },
                    { 9, "memory", "Dyski SSD", 7 },
                    { 11, "memory", "Karty graficzne NVIDIA", 10 },
                    { 12, "memory", "Karty graficzne AMD", 10 },
                    { 16, "phone_android", "Powerbanki", 15 }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Description", "IsHidden", "ManufacturerId", "Model", "Name", "Price", "ProductTimeCreated", "ProductTimeModified", "ProductUniqueId" },
                values: new object[] { 2, 14, "opis do produktu", false, 3, "Peppa-L21B Crush Green", "Huawei P smart 2021", 699m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "000-000-a2" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Description", "IsHidden", "ManufacturerId", "Model", "Name", "Price", "ProductTimeCreated", "ProductTimeModified", "ProductUniqueId" },
                values: new object[] { 1, 4, "<p class='product-description-text'>Gotuj się do walki. Gamingowy laptop Gigabyte AORUS 17G wprowadzi Cię na pola wirtualnych bitew, oddając do dyspozycji arsenał, który poprowadzi Cię do niezliczonych zwycięstw. Wyposażony został w wyselekcjonowane, ultrawydajne komponenty - nowoczesny procesor oraz dedykowaną kartę graficzną. Z takim zapleczem technologicznym Twoi rywale mogą co najwyżej przygotowywać się do odwrotu.</p><img src='/img/Products/000-000-a1/1.jpeg' /><p class='product-description-text'>Wejdź do gry i poczuj smak zwycięstwa dzięki najnowszej platformie RTX z architekturą NVIDIA Ampere. Wyposażono ją w dedykowane rdzenie RT odpowiadającej za ray tracing oraz rdzenie Tensor na potrzeby przetwarzania SI. Wykorzystując technologię głębokiego, maszynowego uczenia się – DLSS (Deep Learning Super Sampling), procesor graficzny zwiększy częstotliwość generowania klatek przy zachowaniu pięknych, ostrych obrazów w grach.</p>'", false, 2, "17G XD-73EE345SH", "Gigabyte AORUS 17G", 10499m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "000-000-a1" });

            migrationBuilder.InsertData(
                table: "ProductsGallery",
                columns: new[] { "Id", "PhotoUrl", "ProductId" },
                values: new object[,]
                {
                    { 5, "/img/Products/000-000-a2/1.jpg", 2 },
                    { 6, "/img/Products/000-000-a2/2.jpg", 2 },
                    { 7, "/img/Products/000-000-a2/3.jpg", 2 }
                });

            migrationBuilder.InsertData(
                table: "ProductsSpecValue",
                columns: new[] { "Id", "ProductId", "SpecValue", "SpecificationId" },
                values: new object[,]
                {
                    { 11, 2, "Kirin 710A", 1 },
                    { 12, 2, "4 GB", 2 },
                    { 13, 2, "128 GB", 3 },
                    { 14, 2, "Mali-G51 MP4", 4 },
                    { 15, 2, "Zielony", 5 },
                    { 16, 2, "Android 10", 6 },
                    { 17, 2, "2400 x 1080", 7 },
                    { 18, 2, "6,67", 8 },
                    { 19, 2, "Wbudowane głośniki stereo", 9 },
                    { 20, 2, "128 GB", 10 }
                });

            migrationBuilder.InsertData(
                table: "Warehouse",
                columns: new[] { "Id", "ProductId", "Quantity" },
                values: new object[] { 2, 2, 12 });

            migrationBuilder.InsertData(
                table: "ProductsGallery",
                columns: new[] { "Id", "PhotoUrl", "ProductId" },
                values: new object[,]
                {
                    { 1, "/img/Products/000-000-a1/1.jpeg", 1 },
                    { 2, "/img/Products/000-000-a1/2.jpeg", 1 },
                    { 3, "/img/Products/000-000-a1/3.jpeg", 1 },
                    { 4, "/img/Products/000-000-a1/4.jpeg", 1 }
                });

            migrationBuilder.InsertData(
                table: "ProductsSpecValue",
                columns: new[] { "Id", "ProductId", "SpecValue", "SpecificationId" },
                values: new object[,]
                {
                    { 1, 1, "Intel Core i7-11800H", 1 },
                    { 2, 1, "32 GB", 2 },
                    { 3, 1, "512 GB", 3 },
                    { 4, 1, "NVIDIA GeForce RTX 3070", 4 },
                    { 5, 1, "Czarny", 5 },
                    { 6, 1, "Windows 10 Home", 6 },
                    { 7, 1, "1920 x 1080 (FullHD)", 7 },
                    { 8, 1, "17,3", 8 },
                    { 9, 1, "Wbudowane głośniki stereo", 9 },
                    { 10, 1, "512 GB", 10 }
                });

            migrationBuilder.InsertData(
                table: "Warehouse",
                columns: new[] { "Id", "ProductId", "Quantity" },
                values: new object[] { 1, 1, 20 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Manufacturers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Manufacturers",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Manufacturers",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Manufacturers",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProductsGallery",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "ProductsSpecValue",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Warehouse",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Warehouse",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ProductsSpecification",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Manufacturers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Manufacturers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AddColumn<bool>(
                name: "IsOnSale",
                table: "Products",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SaleValue",
                table: "Products",
                type: "decimal(10,2)",
                nullable: true);
        }
    }
}
