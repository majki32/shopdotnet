﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DotNetShop.DAL.Migrations
{
    public partial class changingrelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MANUFACT_PRODUCTST_PRODUCTS",
                table: "Manufacturers");

            migrationBuilder.RenameColumn(
                name: "ManufacturerIconName",
                table: "Manufacturers",
                newName: "ManufacturerLogoPath");

            migrationBuilder.RenameIndex(
                name: "ProductsToManufacturers_FK",
                table: "Manufacturers",
                newName: "IX_Manufacturers_ProductId");

            migrationBuilder.RenameColumn(
                name: "CategoryIconPath",
                table: "Categories",
                newName: "CategoryIconName");

            migrationBuilder.AddForeignKey(
                name: "FK_Manufacturers_Products_ProductId",
                table: "Manufacturers",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manufacturers_Products_ProductId",
                table: "Manufacturers");

            migrationBuilder.RenameColumn(
                name: "ManufacturerLogoPath",
                table: "Manufacturers",
                newName: "ManufacturerIconName");

            migrationBuilder.RenameIndex(
                name: "IX_Manufacturers_ProductId",
                table: "Manufacturers",
                newName: "ProductsToManufacturers_FK");

            migrationBuilder.RenameColumn(
                name: "CategoryIconName",
                table: "Categories",
                newName: "CategoryIconPath");

            migrationBuilder.AddForeignKey(
                name: "FK_MANUFACT_PRODUCTST_PRODUCTS",
                table: "Manufacturers",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
