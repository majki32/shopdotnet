﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DotNetShop.DAL.Migrations
{
    public partial class test22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "Warehouse",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Warehouse_ProductId",
                table: "Warehouse",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Warehouse_Products_ProductId",
                table: "Warehouse",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Warehouse_Products_ProductId",
                table: "Warehouse");

            migrationBuilder.DropIndex(
                name: "IX_Warehouse_ProductId",
                table: "Warehouse");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Warehouse");
        }
    }
}
