﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DotNetShop.DAL.Migrations
{
    public partial class changename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductUniqueId",
                table: "ProductsGallery",
                newName: "ProductGalleryUniqueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductGalleryUniqueId",
                table: "ProductsGallery",
                newName: "ProductUniqueId");
        }
    }
}
