﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DotNetShop.DAL.Migrations
{
    public partial class addProductUniqueId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductGalleryUniqueId",
                table: "ProductsGallery");

            migrationBuilder.AddColumn<string>(
                name: "ProductUniqueId",
                table: "Products",
                type: "varchar(40)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductUniqueId",
                table: "Products");

            migrationBuilder.AddColumn<string>(
                name: "ProductGalleryUniqueId",
                table: "ProductsGallery",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
