﻿/*==============================================================*/
/* Table: UsersRole                                             */
/*==============================================================*/
create table UsersRole (
   Id          int      Identity(1,1)            not null,
   UserId              int                  not null,
   Role                 nvarchar(20)          not null,
   constraint PK_USERSROLE primary key (Id)
)
GO
alter table UsersRole
   add constraint FK_USERSROL_USERSTOUS_USERS foreign key (UserId)
      references Users (Id)
GO
/*==============================================================*/
/* Index: UsersToUsersRole_FK                                   */
/*==============================================================*/




create nonclustered index UsersToUsersRole_FK on UsersRole (UserId ASC)