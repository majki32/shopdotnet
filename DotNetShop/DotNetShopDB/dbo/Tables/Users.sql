﻿/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/
create table Users (
   Id          int      Identity(1,1)            not null,
   Login                nvarchar(50)          not null,
   Password             nvarchar(50)          not null,
   Name                 nvarchar(50)          not null,
   Surname              nvarchar(50)          not null,
   EmailAddress         nvarchar(50)          not null,
   constraint PK_USERS primary key (Id)
)