﻿/*==============================================================*/
/* Table: Orders                                                */
/*==============================================================*/
create table Orders (
  Id          int      Identity(1,1)            not null,
   UserId              int                  null,
   OrderStatus          nvarchar(50)          not null,
   DatePurchased        datetime             not null,
   DateShipment         datetime             null,
   DateFinished         datetime             null,
   IsPaid               bit                  not null,
   constraint PK_ORDERS primary key (Id)
)
GO
alter table Orders
   add constraint FK_ORDERS_USERSTOOR_USERS foreign key (UserId)
      references Users (Id)
GO
/*==============================================================*/
/* Index: UsersToOrders_FK                                      */
/*==============================================================*/




create nonclustered index UsersToOrders_FK on Orders (UserId ASC)