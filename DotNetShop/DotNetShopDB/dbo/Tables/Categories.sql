﻿/*==============================================================*/
/* Table: Categories                                            */
/*==============================================================*/
create table Categories (
   Id          int      Identity(1,1)            not null,
   ProductId           int                  null,
   ParentCategoryId      int                  null,
   Name                 nvarchar(50)          not null,
   CategoryIconPath     nvarchar(50)          not null,
   constraint PK_CATEGORIES primary key (Id)
)
GO
alter table Categories
   add constraint FK_CATEGORI_PRODUCTST_PRODUCTS foreign key (ProductId)
      references Products (Id)
GO
alter table Categories
   add constraint FK_CATEGORI_RELATIONS_CATEGORI foreign key (ParentCategoryId)
      references Categories (Id)
GO
/*==============================================================*/
/* Index: ProductsToCategories_FK                               */
/*==============================================================*/




create nonclustered index ProductsToCategories_FK on Categories (ProductId ASC)
GO
/*==============================================================*/
/* Index: Relationship_11_FK                                    */
/*==============================================================*/




create nonclustered index Relationship_11_FK on Categories (ParentCategoryId ASC)