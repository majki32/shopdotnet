﻿/*==============================================================*/
/* Table: Products                                              */
/*==============================================================*/
create table Products (
   Id          int      Identity(1,1)            not null,
   WarehouseId         int                  null,
   ManufacturerId      int                  not null,
   Name                 nvarchar(50)          not null,
   Description          text                 not null,
   Model                nvarchar(50)          not null,
   Price                decimal(10,2)        not null,
   constraint PK_PRODUCTS primary key (Id)
)
GO
alter table Products
   add constraint FK_PRODUCTS_PRODUCTST_MANUFACT foreign key (ManufacturerId)
      references Manufacturers (Id)
GO
alter table Products
   add constraint FK_PRODUCTS_PRODUCTST_WAREHOUS foreign key (WarehouseId)
      references Warehouse (Id)
GO
/*==============================================================*/
/* Index: ProductsToWarehouse_FK                                */
/*==============================================================*/




create nonclustered index ProductsToWarehouse_FK on Products (WarehouseId ASC)
GO
/*==============================================================*/
/* Index: ProductsToManufacturers2_FK                           */
/*==============================================================*/




create nonclustered index ProductsToManufacturers2_FK on Products (ManufacturerId ASC)