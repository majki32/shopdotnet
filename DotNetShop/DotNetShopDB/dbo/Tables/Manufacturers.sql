﻿/*==============================================================*/
/* Table: Manufacturers                                         */
/*==============================================================*/
create table Manufacturers (
   Id          int      Identity(1,1)            not null,
   ProductId           int                  null,
   Name                 nvarchar(50)          not null,
   ManufacturerIconPath nvarchar(50)          not null,
   constraint PK_MANUFACTURERS primary key (Id)
)
GO
alter table Manufacturers
   add constraint FK_MANUFACT_PRODUCTST_PRODUCTS foreign key (ProductId)
      references Products (Id)
GO
/*==============================================================*/
/* Index: ProductsToManufacturers_FK                            */
/*==============================================================*/




create nonclustered index ProductsToManufacturers_FK on Manufacturers (ProductId ASC)