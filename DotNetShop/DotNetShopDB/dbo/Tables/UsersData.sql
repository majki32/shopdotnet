﻿/*==============================================================*/
/* Table: UsersData                                             */
/*==============================================================*/
create table UsersData (
   Id          int      Identity(1,1)            not null,
   UserId              int                  not null,
   CompanyName          nvarchar(50)          null,
   Nip                  nvarchar(20)          null,
   Address              nvarchar(50)          null,
   ZipCode              nvarchar(10)          null,
   HomeNumber           nvarchar(10)          null,
   LocalNumber          nvarchar(10)          null,
   City                 nvarchar(50)          null,
   Phone                nvarchar(15)          null,
   constraint PK_USERSDATA primary key (Id)
)
GO
alter table UsersData
   add constraint FK_USERSDAT_USERSTOUS_USERS foreign key (UserId)
      references Users (Id)
GO
/*==============================================================*/
/* Index: UsersToUsersData_FK                                   */
/*==============================================================*/




create nonclustered index UsersToUsersData_FK on UsersData (UserId ASC)