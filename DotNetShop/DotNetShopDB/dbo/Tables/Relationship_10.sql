﻿/*==============================================================*/
/* Table: Relationship_10                                       */
/*==============================================================*/
create table Relationship_10 (
   Id          int      Identity(1,1)            not null,
   ProductId           int                  not null,
   OrderId             int                  not null,
   constraint PK_RELATIONSHIP_10 primary key (Id, ProductId, OrderId)
)
GO
alter table Relationship_10
   add constraint FK_RELATION_RELATIONS_PRODUCTS foreign key (ProductId)
      references Products (Id)
GO
alter table Relationship_10
   add constraint FK_RELATION_RELATIONS_ORDERS foreign key (OrderId)
      references Orders (Id)
GO
/*==============================================================*/
/* Index: Relationship_10_FK                                    */
/*==============================================================*/




create nonclustered index Relationship_10_FK on Relationship_10 (ProductId ASC)
GO
/*==============================================================*/
/* Index: Relationship_12_FK                                    */
/*==============================================================*/




create nonclustered index Relationship_12_FK on Relationship_10 (OrderId ASC)