﻿/*==============================================================*/
/* Table: Warehouse                                             */
/*==============================================================*/
create table Warehouse (
   Id          int      Identity(1,1)            not null,
   Quantity             int                  not null,
   constraint PK_WAREHOUSE primary key (Id)
)