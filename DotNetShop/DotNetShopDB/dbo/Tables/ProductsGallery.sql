﻿/*==============================================================*/
/* Table: ProductsGallery                                       */
/*==============================================================*/
create table ProductsGallery (
   Id          int      Identity(1,1)            not null,
   ProductId           int                  null,
   PhotoUrl             nvarchar(50)          not null,
   constraint PK_PRODUCTSGALLERY primary key (Id)
)
GO
alter table ProductsGallery
   add constraint FK_PRODUCTS_PRODUCTST_PRODUCTS foreign key (ProductId)
      references Products (Id)
GO
/*==============================================================*/
/* Index: ProductsToProductsGallery_FK                          */
/*==============================================================*/




create nonclustered index ProductsToProductsGallery_FK on ProductsGallery (ProductId ASC)