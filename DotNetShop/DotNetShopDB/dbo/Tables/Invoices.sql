﻿/*==============================================================*/
/* Table: Invoices                                              */
/*==============================================================*/
create table Invoices (
   Id          int      Identity(1,1)            not null,
   OrderId             int                  null,
   Number               nvarchar(50)          not null,
   Value                decimal(10,2)        not null,
   VAT                  int                  not null,
   BankName             nvarchar(50)          not null,
   PaymentMethod        nvarchar(50)          not null,
   constraint PK_INVOICES primary key (Id)
)
GO
alter table Invoices
   add constraint FK_INVOICES_ORDERSTOI_ORDERS foreign key (OrderId)
      references Orders (Id)
GO
/*==============================================================*/
/* Index: OrdersToInvoices_FK                                   */
/*==============================================================*/




create nonclustered index OrdersToInvoices_FK on Invoices (OrderId ASC)