USE [MicomDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentCategoryId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CategoryIconPath] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CATEGORIES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[Number] [varchar](50) NOT NULL,
	[Value] [decimal](10, 2) NOT NULL,
	[VAT] [int] NOT NULL,
	[BankName] [nvarchar](50) NOT NULL,
	[PaymentMethod] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_INVOICES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manufacturers]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manufacturers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ManufacturerIconName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MANUFACTURERS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderElem]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderElem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
 CONSTRAINT [PK_ORDERELEM] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[ProductId] ASC,
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderStatus] [nvarchar](50) NOT NULL,
	[DatePurchased] [datetime] NOT NULL,
	[DateShipment] [datetime] NULL,
	[DateFinished] [datetime] NULL,
	[IsPaid] [bit] NOT NULL,
	[UserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_ORDERS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WarehouseId] [int] NULL,
	[CategoryId] [int] NULL,
	[ManufacturerId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [text] NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
	[Price] [decimal](10, 2) NOT NULL,
	[IsOnSale] [bit] NULL,
	[SaleValue] [decimal](10, 2) NULL,
 CONSTRAINT [PK_PRODUCTS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsGallery]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsGallery](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[PhotoUrl] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PRODUCTSGALLERY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsOpinion]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsOpinion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Opinion] [text] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rating] [smallint] NOT NULL,
	[Helpful] [smallint] NULL,
	[NotHelpful] [smallint] NULL,
	[IsConfirmBuy] [bit] NOT NULL,
 CONSTRAINT [PK_PRODUCTSOPINION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsSpecification]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsSpecification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpecName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PRODUCTSSPECIFICATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsSpecValue]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsSpecValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpecValue] [nvarchar](50) NULL,
	[ProductId] [int] NOT NULL,
	[SpecificationId] [int] NOT NULL,
 CONSTRAINT [PK_PRODUCTSSPECVALUE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warehouse]    Script Date: 25.04.2021 15:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warehouse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_WAREHOUSE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424084839_InitialCreate', N'5.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424084917_IgnoreChanges', N'5.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424113107_AddIdentity', N'5.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424115124_ExtendIdentityUser', N'5.0.5')
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (1, NULL, N'Laptopy', N'cat-1.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (2, NULL, N'Telefony', N'cat-2.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (3, NULL, N'Akumulatory', N'cat-3.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (4, 3, N'Baterie', N'cat-4.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (5, 4, N'Paluszki', N'cat-4.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (6, 3, N'Podzespoły komputerowe', N'cat-5.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (7, 1, N'TV', N'cat-6.png')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Manufacturers] ON 

INSERT [dbo].[Manufacturers] ([Id], [ProductId], [Name], [ManufacturerIconName]) VALUES (1, NULL, N'Philips', N'philips.png')
INSERT [dbo].[Manufacturers] ([Id], [ProductId], [Name], [ManufacturerIconName]) VALUES (2, NULL, N'Sony', N'sony.png')
SET IDENTITY_INSERT [dbo].[Manufacturers] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (3, 2, 1, 2, N'Komputer PC', N'Super komputer do grania', N'ZTE-852', CAST(2000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (4, 1, 5, 1, N'Pralka Bosch', N'Super pralka bosch do prania', N'PXF-212000', CAST(1500.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (5, 1, 1, 1, N'Laptop 1', N'Super Laptop 1', N'PCZ-4580', CAST(1500.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (6, 2, 1, 2, N'Laptop 2', N'Super Laptop 2', N'ZTX-8531', CAST(2000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (7, 1, 1, 1, N'Laptop 3', N'Super Laptop 3', N'HJX21-S5', CAST(4000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (8, 2, 1, 2, N'Laptop 4', N'Super Laptop 4', N'LP85-S44', CAST(5000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (9, 1, 1, 1, N'Laptop 5', N'Super Laptop 5', N'CZP-8415', CAST(15000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (10, 2, 1, 2, N'Laptop 6', N'Super Laptop 6', N'CHP-S21S22', CAST(3490.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (11, 1, 5, 1, N'Laptop 9', N'Super Laptop 9', N'PCZ-4581', CAST(1500.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (12, 2, 5, 2, N'Laptop 10', N'Super Laptop 10', N'ZTX-8535', CAST(2000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (13, 1, 5, 1, N'Laptop 11', N'Super Laptop 11', N'HJX21-S6', CAST(4000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (14, 2, 5, 2, N'Laptop 12', N'Super Laptop 12', N'LP85-S50', CAST(5000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (15, 1, 6, 1, N'Laptop 13', N'Super Laptop 13', N'CZP-8421', CAST(15000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (16, 2, 6, 2, N'Laptop 14', N'Super Laptop 14', N'CHP-S21S35', CAST(3490.00 AS Decimal(10, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductsGallery] ON 

INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (1, 4, N'prod-2.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (2, 5, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (3, 6, N'prod-4.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (4, 7, N'prod-5.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (5, 8, N'prod-1.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (6, 9, N'prod-2.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (7, 10, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (8, 3, N'prod-4.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (9, 11, N'prod-2.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (10, 12, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (11, 13, N'prod-4.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (12, 14, N'prod-5.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (13, 15, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (14, 16, N'prod-4.jpg')
SET IDENTITY_INSERT [dbo].[ProductsGallery] OFF
GO
SET IDENTITY_INSERT [dbo].[Warehouse] ON 

INSERT [dbo].[Warehouse] ([Id], [Quantity]) VALUES (1, 100)
INSERT [dbo].[Warehouse] ([Id], [Quantity]) VALUES (2, 50)
SET IDENTITY_INSERT [dbo].[Warehouse] OFF
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_CATEGORI_RELATIONS_CATEGORI] FOREIGN KEY([ParentCategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [FK_CATEGORI_RELATIONS_CATEGORI]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_INVOICES_ORDERSTOI_ORDERS] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_INVOICES_ORDERSTOI_ORDERS]
GO
ALTER TABLE [dbo].[Manufacturers]  WITH CHECK ADD  CONSTRAINT [FK_MANUFACT_PRODUCTST_PRODUCTS] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Manufacturers] CHECK CONSTRAINT [FK_MANUFACT_PRODUCTST_PRODUCTS]
GO
ALTER TABLE [dbo].[OrderElem]  WITH CHECK ADD  CONSTRAINT [FK_ORDERELE_RELATIONS_ORDERS] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderElem] CHECK CONSTRAINT [FK_ORDERELE_RELATIONS_ORDERS]
GO
ALTER TABLE [dbo].[OrderElem]  WITH CHECK ADD  CONSTRAINT [FK_ORDERELE_RELATIONS_PRODUCTS] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[OrderElem] CHECK CONSTRAINT [FK_ORDERELE_RELATIONS_PRODUCTS]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_AspNetUsers_AppUserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_AspNetUsers_AppUserId]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_CATEGORI] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_CATEGORI]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_MANUFACT] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_MANUFACT]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_WAREHOUS] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[Warehouse] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_WAREHOUS]
GO
ALTER TABLE [dbo].[ProductsGallery]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_PRODUCTS] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductsGallery] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_PRODUCTS]
GO
ALTER TABLE [dbo].[ProductsOpinion]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION2] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductsOpinion] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION2]
GO
ALTER TABLE [dbo].[ProductsSpecValue]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPEC] FOREIGN KEY([SpecificationId])
REFERENCES [dbo].[ProductsSpecification] ([Id])
GO
ALTER TABLE [dbo].[ProductsSpecValue] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPEC]
GO
ALTER TABLE [dbo].[ProductsSpecValue]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductsSpecValue] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION]
GO
