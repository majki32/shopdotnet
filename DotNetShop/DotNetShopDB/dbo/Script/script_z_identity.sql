USE [MicomDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentCategoryId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CategoryIconPath] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CATEGORIES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[Number] [varchar](50) NOT NULL,
	[Value] [decimal](10, 2) NOT NULL,
	[VAT] [int] NOT NULL,
	[BankName] [nvarchar](50) NOT NULL,
	[PaymentMethod] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_INVOICES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manufacturers]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manufacturers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ManufacturerIconName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MANUFACTURERS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderElem]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderElem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
 CONSTRAINT [PK_ORDERELEM] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[ProductId] ASC,
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderStatus] [nvarchar](50) NOT NULL,
	[DatePurchased] [datetime] NOT NULL,
	[DateShipment] [datetime] NULL,
	[DateFinished] [datetime] NULL,
	[IsPaid] [bit] NOT NULL,
	[UserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_ORDERS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WarehouseId] [int] NULL,
	[CategoryId] [int] NULL,
	[ManufacturerId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [text] NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
	[Price] [decimal](10, 2) NOT NULL,
	[IsOnSale] [bit] NULL,
	[SaleValue] [decimal](10, 2) NULL,
 CONSTRAINT [PK_PRODUCTS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsGallery]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsGallery](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[PhotoUrl] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PRODUCTSGALLERY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsOpinion]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsOpinion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Opinion] [text] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rating] [smallint] NOT NULL,
	[Helpful] [smallint] NULL,
	[NotHelpful] [smallint] NULL,
	[IsConfirmBuy] [bit] NOT NULL,
 CONSTRAINT [PK_PRODUCTSOPINION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsSpecification]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsSpecification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpecName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PRODUCTSSPECIFICATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductsSpecValue]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsSpecValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpecValue] [nvarchar](50) NULL,
	[ProductId] [int] NOT NULL,
	[SpecificationId] [int] NOT NULL,
 CONSTRAINT [PK_PRODUCTSSPECVALUE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserData]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NULL,
	[CompanyName] [nvarchar](50) NULL,
	[Nip] [varchar](20) NULL,
	[Address] [nvarchar](50) NULL,
	[ZipCodeAndCity] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warehouse]    Script Date: 25.04.2021 15:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warehouse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_WAREHOUSE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424084839_InitialCreate', N'5.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424084917_IgnoreChanges', N'5.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424113107_AddIdentity', N'5.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210424115124_ExtendIdentityUser', N'5.0.5')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'3ab19ce4-9c12-4126-9639-f8999b7e8839', N'User', N'USER', N'781f510e-eeaa-4335-827f-e7db85c0294f')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'd0849be8-1d59-425a-a014-56f50402e9b1', N'Admin', N'ADMIN', N'8351bbd5-e9c4-43bd-8650-9f80a61e37a5')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1d8ca7fa-04ca-4c1d-a620-ed807b87d35c', N'3ab19ce4-9c12-4126-9639-f8999b7e8839')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4261c5e0-5f2a-48ab-8854-60b7ef74123c', N'3ab19ce4-9c12-4126-9639-f8999b7e8839')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'51d21c56-6036-4b80-b4d6-5dbc00fff48f', N'3ab19ce4-9c12-4126-9639-f8999b7e8839')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd04eed8a-410e-4043-8d2a-3e1beda30e1e', N'3ab19ce4-9c12-4126-9639-f8999b7e8839')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'53f9ee4c-1f8d-47ea-accb-445c0a4a3713', N'd0849be8-1d59-425a-a014-56f50402e9b1')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'1d8ca7fa-04ca-4c1d-a620-ed807b87d35c', N'antekszmykiewicz', N'ANTEKSZMYKIEWICZ', N'test3@test1.com', N'TEST3@TEST1.COM', 0, N'AQAAAAEAACcQAAAAEB10W4X+4cmVDjrkD9lKRi1MhW0x2ehRYz5SoXFa9GT5wUzdvlH3wLwnXFz00mZyHA==', N'UO2G2QZ3TUGHVWWNVIQOP7DSLNJUTLPK', N'ca95d6ba-2e38-41b8-bac3-dd8994b0eeba', NULL, 0, 0, NULL, 1, 0, N'Antek', N'Szmykiewicz')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'4261c5e0-5f2a-48ab-8854-60b7ef74123c', N'mikol32', N'MIKOL32', N'test1@test1.com', N'TEST1@TEST1.COM', 0, N'AQAAAAEAACcQAAAAEN0lD4A9+63NsFl+Cv1NWPiK499SntvWgWea/a3Mw6nMoPSCbldIcFjTP/FjFfnSmg==', N'AKQWN4GBRRN5X26D3KYYBSE52OF7G345', N'55f0fff4-0604-4888-8a64-2f941b884c1f', NULL, 0, 0, NULL, 1, 0, NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'51d21c56-6036-4b80-b4d6-5dbc00fff48f', N'test2', N'TEST2', N'test2@test1.com', N'TEST2@TEST1.COM', 0, N'AQAAAAEAACcQAAAAEHeEWywc78m716EJVMPAQ4p9zamcRksF0UFBpNFGpXuSfSC4AQInwxg0fieFHZa7ug==', N'GACWHOGA6VLWQWU6N7HC2CNFDASG6SF6', N'c2869cc9-e61a-46cb-93ff-89b2ebb7a466', NULL, 0, 0, NULL, 1, 0, N'Adam', N'Kowalski')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'53f9ee4c-1f8d-47ea-accb-445c0a4a3713', N'Admin', N'ADMIN', N'admin@admin.com', N'ADMIN@ADMIN.COM', 0, N'AQAAAAEAACcQAAAAEB2/wouivLgCCgOA6qNyn3kHNGAOg4ym1yKNSjTXxnjQF6aQF64ACAT+KZDVLm0LhA==', N'HWRCBS2WLZUN7XE3DVF3ERLQY6QO2R5G', N'8834e4bb-eff2-423a-b675-47c7b0efc5d0', NULL, 0, 0, NULL, 1, 0, N'Admin', N'Admin')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'd04eed8a-410e-4043-8d2a-3e1beda30e1e', N'mikol1', N'MIKOL1', N'mikol1@mikol.com', N'MIKOL1@MIKOL.COM', 0, N'AQAAAAEAACcQAAAAEHMNGZxtN0shkJ1hEBtQ4/zUXAF4tDpoN4LBLRwtIKChvNwFR/ZjoBUS3DWilajNbQ==', N'ZL4EV2BMZZRHU7DLSWPNH6Y7BXUFBUCK', N'e2d71f6a-c070-4325-9730-0241eebd0952', NULL, 0, 0, NULL, 1, 0, N'Andrzej', N'Postura')
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (1, NULL, N'Laptopy', N'cat-1.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (2, NULL, N'Telefony', N'cat-2.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (3, NULL, N'Akumulatory', N'cat-3.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (4, 3, N'Baterie', N'cat-4.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (5, 4, N'Paluszki', N'cat-4.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (6, 3, N'Podzespoły komputerowe', N'cat-5.png')
INSERT [dbo].[Categories] ([Id], [ParentCategoryId], [Name], [CategoryIconPath]) VALUES (7, 1, N'TV', N'cat-6.png')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Manufacturers] ON 

INSERT [dbo].[Manufacturers] ([Id], [ProductId], [Name], [ManufacturerIconName]) VALUES (1, NULL, N'Philips', N'philips.png')
INSERT [dbo].[Manufacturers] ([Id], [ProductId], [Name], [ManufacturerIconName]) VALUES (2, NULL, N'Sony', N'sony.png')
SET IDENTITY_INSERT [dbo].[Manufacturers] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (3, 2, 1, 2, N'Komputer PC', N'Super komputer do grania', N'ZTE-852', CAST(2000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (4, 1, 5, 1, N'Pralka Bosch', N'Super pralka bosch do prania', N'PXF-212000', CAST(1500.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (5, 1, 1, 1, N'Laptop 1', N'Super Laptop 1', N'PCZ-4580', CAST(1500.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (6, 2, 1, 2, N'Laptop 2', N'Super Laptop 2', N'ZTX-8531', CAST(2000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (7, 1, 1, 1, N'Laptop 3', N'Super Laptop 3', N'HJX21-S5', CAST(4000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (8, 2, 1, 2, N'Laptop 4', N'Super Laptop 4', N'LP85-S44', CAST(5000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (9, 1, 1, 1, N'Laptop 5', N'Super Laptop 5', N'CZP-8415', CAST(15000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (10, 2, 1, 2, N'Laptop 6', N'Super Laptop 6', N'CHP-S21S22', CAST(3490.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (11, 1, 5, 1, N'Laptop 9', N'Super Laptop 9', N'PCZ-4581', CAST(1500.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (12, 2, 5, 2, N'Laptop 10', N'Super Laptop 10', N'ZTX-8535', CAST(2000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (13, 1, 5, 1, N'Laptop 11', N'Super Laptop 11', N'HJX21-S6', CAST(4000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (14, 2, 5, 2, N'Laptop 12', N'Super Laptop 12', N'LP85-S50', CAST(5000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (15, 1, 6, 1, N'Laptop 13', N'Super Laptop 13', N'CZP-8421', CAST(15000.00 AS Decimal(10, 2)), NULL, NULL)
INSERT [dbo].[Products] ([Id], [WarehouseId], [CategoryId], [ManufacturerId], [Name], [Description], [Model], [Price], [IsOnSale], [SaleValue]) VALUES (16, 2, 6, 2, N'Laptop 14', N'Super Laptop 14', N'CHP-S21S35', CAST(3490.00 AS Decimal(10, 2)), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductsGallery] ON 

INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (1, 4, N'prod-2.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (2, 5, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (3, 6, N'prod-4.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (4, 7, N'prod-5.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (5, 8, N'prod-1.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (6, 9, N'prod-2.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (7, 10, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (8, 3, N'prod-4.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (9, 11, N'prod-2.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (10, 12, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (11, 13, N'prod-4.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (12, 14, N'prod-5.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (13, 15, N'prod-3.jpg')
INSERT [dbo].[ProductsGallery] ([Id], [ProductId], [PhotoUrl]) VALUES (14, 16, N'prod-4.jpg')
SET IDENTITY_INSERT [dbo].[ProductsGallery] OFF
GO
SET IDENTITY_INSERT [dbo].[Warehouse] ON 

INSERT [dbo].[Warehouse] ([Id], [Quantity]) VALUES (1, 100)
INSERT [dbo].[Warehouse] ([Id], [Quantity]) VALUES (2, 50)
SET IDENTITY_INSERT [dbo].[Warehouse] OFF
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_CATEGORI_RELATIONS_CATEGORI] FOREIGN KEY([ParentCategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [FK_CATEGORI_RELATIONS_CATEGORI]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_INVOICES_ORDERSTOI_ORDERS] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_INVOICES_ORDERSTOI_ORDERS]
GO
ALTER TABLE [dbo].[Manufacturers]  WITH CHECK ADD  CONSTRAINT [FK_MANUFACT_PRODUCTST_PRODUCTS] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Manufacturers] CHECK CONSTRAINT [FK_MANUFACT_PRODUCTST_PRODUCTS]
GO
ALTER TABLE [dbo].[OrderElem]  WITH CHECK ADD  CONSTRAINT [FK_ORDERELE_RELATIONS_ORDERS] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderElem] CHECK CONSTRAINT [FK_ORDERELE_RELATIONS_ORDERS]
GO
ALTER TABLE [dbo].[OrderElem]  WITH CHECK ADD  CONSTRAINT [FK_ORDERELE_RELATIONS_PRODUCTS] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[OrderElem] CHECK CONSTRAINT [FK_ORDERELE_RELATIONS_PRODUCTS]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_AspNetUsers_AppUserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_AspNetUsers_AppUserId]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_CATEGORI] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_CATEGORI]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_MANUFACT] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[Manufacturers] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_MANUFACT]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_WAREHOUS] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[Warehouse] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_WAREHOUS]
GO
ALTER TABLE [dbo].[ProductsGallery]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTST_PRODUCTS] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductsGallery] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTST_PRODUCTS]
GO
ALTER TABLE [dbo].[ProductsOpinion]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION2] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductsOpinion] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION2]
GO
ALTER TABLE [dbo].[ProductsSpecValue]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPEC] FOREIGN KEY([SpecificationId])
REFERENCES [dbo].[ProductsSpecification] ([Id])
GO
ALTER TABLE [dbo].[ProductsSpecValue] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPEC]
GO
ALTER TABLE [dbo].[ProductsSpecValue]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductsSpecValue] CHECK CONSTRAINT [FK_PRODUCTS_PRODUCTSVALUE__PRODUCTSSPECIFICATION]
GO
ALTER TABLE [dbo].[UserData]  WITH CHECK ADD  CONSTRAINT [FK_UserData_AspNetUsers_AspUsers_To_UsersData] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserData] CHECK CONSTRAINT [FK_UserData_AspNetUsers_AspUsers_To_UsersData]
GO
