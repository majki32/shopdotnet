using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BundlerMinifier.TagHelpers;
using DotNetShop.DAL.Model;
using DotNetShop.Extensions;
using DotNetShop.Interfaces;
using DotNetShop.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog.Extensions.Logging.File;

namespace DotNetShop
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            //services.AddMemoryCache();
            services.AddDbContext<MicomDBContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("secondConnectionString"));
            });
            services.AddIdentity<AppUser, IdentityRole>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.User.RequireUniqueEmail = true;
                options.SignIn.RequireConfirmedAccount = true;
            }).AddEntityFrameworkStores<MicomDBContext>().AddTokenProvider<DataProtectorTokenProvider<AppUser>>(TokenOptions.DefaultProvider);
            services.AddScoped<IMailSender, Sender>();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllersWithViews();
            services.AddDistributedMemoryCache();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSession();
            services.AddBundles(options => 
            {
                options.AppendVersion = true;
            });
            //    options => {
            //    options.IdleTimeout = TimeSpan.FromSeconds(10);
            //}

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHttpContextAccessor accessor, MicomDBContext dbContext, UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            loggerFactory.AddFile("Logs/logs-{Date}.txt");
            app.UseSession();
            app.UseResponseCaching();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            dbContext.Database.EnsureCreated();
            DatabaseInitializer.SeedIdentity(userManager, roleManager);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "allcategories",
                    pattern: "kategorie/wszystkie-{mainCategoryId}",
                    defaults: new { controller = "Store", action = "AllCategories" });
                endpoints.MapControllerRoute(
                    name: "productdetail",
                    pattern: "produkty/produkt-{id}",
                    defaults: new { controller = "Store", action = "Details" });
                endpoints.MapControllerRoute(
                    name: "productlist",
                    pattern: "kategorie/kategoria-{categoryId}",
                    defaults: new { controller = "Store", action = "List" });
                endpoints.MapControllerRoute(
                    name: "cart",
                    pattern: "koszyk",
                    defaults: new { controller = "Cart", action = "Cart" });
                endpoints.MapControllerRoute(
                    name: "checkout",
                    pattern: "koszyk/zbieranie-informacji",
                    defaults: new { controller = "Cart", action = "Checkout" });
                endpoints.MapControllerRoute(
                    name: "confirmorder",
                    pattern: "koszyk/podsumowanie",
                    defaults: new { controller = "Cart", action = "ConfirmOrder" });
                endpoints.MapControllerRoute(
                    name: "ordersummary",
                    pattern: "zamowienia/podsumowanie-{orderId}",
                    defaults: new { controller = "Cart", action = "OrderSummary" });
                endpoints.MapControllerRoute(
                    name: "userorder",
                    pattern: "zarzadzanie/zamowienia",
                    defaults: new { controller = "Manage", action = "UserOrders" });
                endpoints.MapControllerRoute(
                    name: "usersettings",
                    pattern: "zarzadzanie/ustawienia-uzytkownika",
                    defaults: new { controller = "Manage", action = "UserSettings" });
                endpoints.MapControllerRoute(
                    name: "addproductspecification",
                    pattern: "zarzadzanie/specyfikacje",
                    defaults: new { controller = "Manage", action = "AddProductSpecification" });
                endpoints.MapControllerRoute(
                    name: "addproduct",
                    pattern: "zarzadzanie/produkty",
                    defaults: new { controller = "Manage", action = "AddProduct" });
                endpoints.MapControllerRoute(
                    name: "addcategory",
                    pattern: "zarzadzanie/kategorie",
                    defaults: new { controller = "Manage", action = "AddCategory" });
                endpoints.MapControllerRoute(
                    name: "addmanufacturer",
                    pattern: "zarzadzanie/producenci",
                    defaults: new { controller = "Manage", action = "AddManufacturer" });
                endpoints.MapControllerRoute(
                    name: "hiddenproducts",
                    pattern: "zarzadzanie/ukryte-produkty",
                    defaults: new { controller = "Manage", action = "HiddenProducts" });
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
