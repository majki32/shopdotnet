﻿using DotNetShop.DAL.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.ViewComponents
{
    public class NavViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            MicomDBContext _dbContext = new MicomDBContext();
            var cat = await _dbContext.Categories.ToListAsync();
            return View(cat);
        }
    }
}
