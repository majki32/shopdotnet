﻿using DotNetShop.Helpers;
using DotNetShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.ViewComponents
{
    public class CartCounterViewComponent : ViewComponent
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private ShoppingCartManager shoppingCartManager;
        public CartCounterViewComponent(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            shoppingCartManager = new ShoppingCartManager(_session);
        }
        public IViewComponentResult Invoke()
        {
            var countItems = shoppingCartManager.GetCartItemsCount();
            return View(countItems);
        }
    }
}
