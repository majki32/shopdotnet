﻿using DotNetShop.DAL.Model;
using DotNetShop.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.ViewComponents
{
    public class UserPanelViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
                return View();
        }
    }
}
