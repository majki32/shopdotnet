﻿using DotNetShop.DAL.Model;
using DotNetShop.Helpers;
using DotNetShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.ViewComponents
{
    public class QuickBasketViewComponent : ViewComponent
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private ShoppingCartManager shoppingCartManager;
        private MicomDBContext dbContext = new MicomDBContext();
        
        public QuickBasketViewComponent(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            shoppingCartManager = new ShoppingCartManager(_session);
        }
        public IViewComponentResult Invoke()
        {
            var cartItems = _session.GetCart();
            var cartTotalPrice = shoppingCartManager.GetCartTotalPrice();
            var cartItemsPhotos = dbContext.ProductsGalleries.Include(pr => pr.Product).ToList();

            CartViewModel cartVM = new CartViewModel()
            {
                CartItems = cartItems,
                ProductPhoto = cartItemsPhotos,
                TotalPrice = cartTotalPrice
            };

            return View(cartVM);
        }
    }
}
