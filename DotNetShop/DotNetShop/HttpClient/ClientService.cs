﻿using MWM.Base.Services.Api;
using MWM.Base.Services.Globals;
using MWM.Commons.Models;
using MWM.Commons.Models.Auth;
using MWM.MainApp.Models;
using MWM.MainApp.Models.Orders;
using MWM.MainApp.Models.Sales;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MWM.MainApp.Api
{
    public class ClientService : BaseClientService
    {
        private static string _baseUri = Global.Instance.ApiUrl;

        #region GET

        public static async Task<TestModel> TestHotline(string dupa)
        {
            return await GetResultAsync<TestModel>(_baseUri, $"Hotline/Test/{dupa}"); //test model musi zgrywac sie z tym co mi zwroci api
        }

        
        #endregion

        #region POST
        public static async Task TestPost(TestModel test)
        {
            await PostAsync<TestModel, HttpResponseMessage>(_baseUri, "Auth/PostTest", test);
        }
        
        #endregion
    }
}
