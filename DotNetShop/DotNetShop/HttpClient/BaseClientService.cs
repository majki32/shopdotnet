﻿using MWM.Base.Services.Globals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MWM.Base.Services.Api
{
    public class BaseClientService
    {
        protected static async Task<T> GetResultAsync<T>(string apiBaseUrl, string apiMethod) where T : class, new()
        {
            using (var client = new HttpClient())
            {
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{apiBaseUrl}{apiMethod}"),
                    Headers = {
                        { HttpRequestHeader.Authorization.ToString(), $"Bearer {Global.Instance.ApiToken}" },
                        { HttpRequestHeader.Accept.ToString(), "application/json" },
                        { "X-Version", "1" }
                    },
                    //Content = new StringContent(JsonConvert.SerializeObject(svm))
                };

                var response = await client.SendAsync(httpRequestMessage);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    try
                    {
                        var result = await JsonSerializer.DeserializeAsync<T>(await response.Content.ReadAsStreamAsync());
                        return result;
                    }
                    catch
                    { 
                        return new T(); 
                    }
                }
                else
                {
                    throw new Exception("Internal server Error");
                }
            }
        }
        protected static async Task<TOut> PostAsync<TIn, TOut>(string apiBaseUrl, string apiMethod, TIn paramsModel, IEnumerable<KeyValuePair<string, string>> urlEncodedParams = null) where TOut : class, new()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiBaseUrl);

                if (urlEncodedParams != null)
                {
                    using (var content = new FormUrlEncodedContent(urlEncodedParams))
                    {
                        var httpRequestMessage = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = new Uri($"{apiBaseUrl}{apiMethod}"),
                            Headers = {
                                { HttpRequestHeader.Authorization.ToString(), $"Bearer {Global.Instance.ApiToken}" },
                                { HttpRequestHeader.Accept.ToString(), "application/x-www-form-urlencoded" },
                                { "X-Version", "1" }
                            },
                            Content = content
                        };

                        var response = await client.SendAsync(httpRequestMessage);

                        try
                        {
                            var result = await JsonSerializer.DeserializeAsync<TOut>(await response.Content.ReadAsStreamAsync());

                            return result;
                        }
                        catch
                        {
                            return new TOut();
                        }
                    }
                }
                else
                {
                    using (var stream = new MemoryStream())
                    {
                        await JsonSerializer.SerializeAsync<TIn>(stream, paramsModel);
                        
                        using (var reader = new StreamReader(stream))
                        {
                            stream.Seek(0, SeekOrigin.Begin);
                            stream.Position = 0;
                            string content = await reader.ReadToEndAsync();
                            var httpRequestMessage = new HttpRequestMessage
                            {
                                Method = HttpMethod.Post,
                                RequestUri = new Uri($"{apiBaseUrl}{apiMethod}"),
                                Headers = {
                                    { HttpRequestHeader.Authorization.ToString(), $"Bearer {Global.Instance.ApiToken}" },
                                    { HttpRequestHeader.Accept.ToString(), "application/json" },
                                    { "X-Version", "1" }
                                },
                                Content = new StringContent(content, Encoding.UTF8, "application/json")
                            };
                            var response = await client.SendAsync(httpRequestMessage);

                            try
                            {
                                var result = await JsonSerializer.DeserializeAsync<TOut>(await response.Content.ReadAsStreamAsync());

                                return result;
                            }
                            catch 
                            {
                                return new TOut();
                            }
                        }
                    }
                }
            }
        }
    }
}
