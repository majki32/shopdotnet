﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MWM.Base.Services.Globals
{
    public class Global
    {
        private static readonly Global instance = new Global();
        public static Global Instance
        {
            get
            {
                return instance;
            }
        }
        private Global() { }
        public string ApiUrl { get; set; } // www.peju.com
        public string ApiToken { get; set; } // token do autoryzacji
        public string Language { get; set; } //
    }
}
