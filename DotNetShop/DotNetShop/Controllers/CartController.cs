﻿using DotNetShop.DAL.Model;
using DotNetShop.Helpers;
using DotNetShop.Interfaces;
using DotNetShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DotNetShop.Controllers
{
    public class CartController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private readonly UserManager<AppUser> _userManager;
        private ShoppingCartManager shoppingCartManager;
        private MicomDBContext dbContext = new MicomDBContext();
        private readonly IMailSender _mailSender;
        public CartController(IHttpContextAccessor httpContextAccessor, UserManager<AppUser> userManager, IMailSender mailSender)
        {
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            shoppingCartManager = new ShoppingCartManager(_session);
            _mailSender = mailSender;
        }
        public IActionResult Cart()
        {
            var cartItems = HttpContext.Session.GetCart();
            var cartTotalPrice = shoppingCartManager.GetCartTotalPrice();
            var cartItemsPhotos = dbContext.ProductsGalleries.Include(pr => pr.Product).ToList();

            CartViewModel cartVM = new CartViewModel()
            {
                CartItems = cartItems,
                ProductPhoto = cartItemsPhotos,
                TotalPrice = cartTotalPrice
            };

            return View(cartVM);
        }
        public IActionResult AddToCart(int id)
        {
            var checkProductQuantity = dbContext.Warehouses.Where(x => x.ProductId == id).FirstOrDefault().Quantity;
            if (checkProductQuantity == 0) return Redirect(Request.Headers["Referer"].ToString());
            shoppingCartManager.AddToCart(id);
            return Redirect(Request.Headers["Referer"].ToString());
        }

        public IActionResult RemoveFromCart(int productId)
        {
            int itemCount = shoppingCartManager.RemoveFromCart(productId);
            int cartItemsCount = shoppingCartManager.GetCartItemsCount();
            decimal cartItemPrice = shoppingCartManager.GetCartItemPrice(itemCount, productId);
            decimal cartTotal = shoppingCartManager.GetCartTotalPrice();
            var result = new CartRemoveViewModel
            {
                RemoveItemId = productId,
                RemovedItemCount = itemCount,
                CartTotal = cartTotal,
                CartItemsCount = cartItemsCount,
                CartItemPrice = cartItemPrice
            };
            return new JsonResult(result);
        }
        [Authorize]
        public async Task<IActionResult> Checkout()
        {
            if (shoppingCartManager.GetCartItemsCount() == 0) return RedirectToAction("Index", "Home");
            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            var user = await _userManager.Users.Include(x => x.UserData).SingleAsync(x => x.Id == userId);

            var order = new Order()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = user.UserData.Address,
                ZipCode = user.UserData.ZipCode,
                City = user.UserData.City
            };

            return View(order);
        }
        [HttpPost]
        public IActionResult Checkout(Order orderdetails)
        {
            if(ModelState.IsValid)
            {
                var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
                shoppingCartManager.CreateOrder(orderdetails, userId);

                var newOrderMail = new Sender()
                {
                    EmailType = EmailEnums.NewOrder,
                    FirstName = orderdetails.FirstName,
                    SendTo = orderdetails.Email,
                    Subject = $"Potwierdzenie zamówienia nr {orderdetails.Id}"
                };

                _mailSender.SendMail(newOrderMail);

                return RedirectToAction("ConfirmOrder");
            }
            else
            {
                return View(orderdetails);
            }
        }
        [Authorize]
        public IActionResult ConfirmOrder()
        {          
            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            var userOrder = dbContext.Orders.Include(x => x.OrderElems).ThenInclude(x => x.Product).ThenInclude(x => x.ProductsGalleries).Where(x => x.UserId == userId).OrderBy(o => o.Id).Last();

            return View(GetUserInformationAboutOrder(userOrder));
        }
        [HttpPost]
        public IActionResult ConfirmOrder(Order orderdetails)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;

                return RedirectToAction("ConfirmOrder");
            }
            else
            {
                return View();
            }
        }

        [Authorize]
        public IActionResult OrderSummary(int orderId)
        {
            IEnumerable<Order> userOrder;
            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            bool isAdmin = User.IsInRole("Admin");
            ViewBag.UserIsAdmin = isAdmin;
            if (ViewBag.UserIsAdmin)
            {
                userOrder = dbContext.Orders.Include(x => x.OrderElems).ThenInclude(x => x.Product).ThenInclude(x => x.ProductsGalleries).Where(x => x.Id == orderId);
            }
            else
            {
                userOrder = dbContext.Orders.Include(x => x.OrderElems).ThenInclude(x => x.Product).ThenInclude(x => x.ProductsGalleries).Where(x => x.UserId == userId && x.Id == orderId);
            }

            if (userOrder == null || !userOrder.Any()) return RedirectToAction("Index", "Home");
            return View(userOrder);
        }
        public Order GetUserInformationAboutOrder(Order order)
        {
            var orderResult = new Order
            {
                Address = order.Address,
                PaymentMethod = order.PaymentMethod,
                LastName = order.LastName,
                FirstName = order.FirstName,
                Email = order.Email,
                PhoneNumber = order.PhoneNumber,
                TotalPrice = order.TotalPrice,
                ZipCode = order.ZipCode,
                City = order.City,
                Comment = order.Comment,
                DatePurchased = order.DatePurchased,
                IsPaid = order.IsPaid,
                OrderElems = order.OrderElems
            };
            return orderResult;
        }
    }
}
