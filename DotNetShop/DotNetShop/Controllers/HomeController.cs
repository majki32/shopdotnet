﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DotNetShop.Models;
using DotNetShop.DAL.Model;
using Microsoft.EntityFrameworkCore;

namespace DotNetShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MicomDBContext dbContext = new MicomDBContext();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            /*using (var dbContext = new MicomDatabaseContext())
            {
                //    var cat = new List<Category>
                //    {
                //        new Category() { Name = "Laptopy", CategoryIconName = "cat-1.png" },
                //        new Category() { Name = "Telefony", CategoryIconName = "cat-2.png" },
                //        new Category() { Name = "Akumulatory", CategoryIconName = "cat-3.png" },
                //        new Category() { Name = "Baterie", CategoryIconName = "cat-4.png", ParentCategoryId = 3 },
                //        new Category() { Name = "Paluszki", CategoryIconName = "cat-4.png", ParentCategoryId = 4 },
                //        new Category() { Name = "Podzespoły komputerowe", CategoryIconName = "cat-5.png", ParentCategoryId = 3 },
                //        new Category() { Name = "TV", CategoryIconName = "cat-6.png", ParentCategoryId = 1}
                //    };


                //    var manu = new List<Manufacturer> { 
                //        new Manufacturer() { Name = "Philips", ManufacturerIconName = "philips.png" },
                //        new Manufacturer() { Name = "Sony", ManufacturerIconName = "sony.png" }
                //    };
                //    manu.ForEach(b => dbContext.Manufacturers.Add(b));
                //    dbContext.SaveChanges();

                var prod = new List<Product>
                {
                    new Product() { Name = "Laptop 9", Description = "Super Laptop 9", Model = "PCZ-4581", Price = 1500, ManufacturerId = 1, WarehouseId = 1, CategoryId = 5},
                    new Product() { Name = "Laptop 10", Description = "Super Laptop 10", Model = "ZTX-8535", Price = 2000, ManufacturerId = 2, WarehouseId = 2, CategoryId = 5 },
                    new Product() { Name = "Laptop 11", Description = "Super Laptop 11", Model = "HJX21-S6", Price = 4000, ManufacturerId = 1, WarehouseId = 1, CategoryId = 5},
                    new Product() { Name = "Laptop 12", Description = "Super Laptop 12", Model = "LP85-S50", Price = 5000, ManufacturerId = 2, WarehouseId = 2, CategoryId = 5 },
                    new Product() { Name = "Laptop 13", Description = "Super Laptop 13", Model = "CZP-8421", Price = 15000, ManufacturerId = 1, WarehouseId = 1, CategoryId = 6},
                    new Product() { Name = "Laptop 14", Description = "Super Laptop 14", Model = "CHP-S21S35", Price = 3490, ManufacturerId = 2, WarehouseId = 2, CategoryId = 6 }
                };
                prod.ForEach(a => dbContext.Products.Add(a));
                dbContext.SaveChanges();
            }*/
            var products = dbContext.Products.Include(x => x.ProductsSpecValues)
                .ThenInclude(x => x.Specification)
                .Include(x => x.ProductsGalleries)
                .Where(x => x.IsHidden == false)
                .OrderBy(x => Guid.NewGuid())
                .Take(8);
            var takeLastProductBoughts = dbContext.OrderElems.Select(g => new { g.ProductId, g.OrderId}).OrderByDescending(g => g.OrderId).Take(3);
            var sumProductsByBoughts = dbContext.OrderElems
                .GroupBy(x => x.ProductId)
                .OrderByDescending(x => x.Sum(x => x.Quantity))
                .Select(g => new
                {
                    g.Key,
                    SUM = g.Sum(x => x.Quantity)
                }).Take(3);
            IList<Product> topBoughtProducts = new List<Product>();
            IList<Product> lastProductBuys = new List<Product>();
            foreach (var countedSum in sumProductsByBoughts)
            {
                var topBoughtProduct = dbContext.Products.Where(x => x.Id == countedSum.Key).ToList();
                foreach (var items in topBoughtProduct)
                {
                    topBoughtProducts.Add(items);
                }
            }
            foreach (var lastBuys in takeLastProductBoughts)
            {
                var lastBoughtProduct = dbContext.Products.Where(x => x.Id == lastBuys.ProductId).ToList();
                foreach (var items in lastBoughtProduct)
                {
                    lastProductBuys.Add(items);
                }
            }
            var indexProducts = new HomeViewModel
            {
                Products = products,
                TopProductBuys = topBoughtProducts,
                LastProductBuys = lastProductBuys
            };
            return View(indexProducts);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
