﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DotNetShop.DAL.Model;
using DotNetShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace DotNetShop.Controllers
{
    public class ManageController : Controller
    {
        private readonly MicomDBContext dbContext = new MicomDBContext();
        private IWebHostEnvironment _webHostEnvironment;
        private UserManager<AppUser> _userManager;
        public ManageController(UserManager<AppUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }
        [Authorize]
        public IActionResult UserOrders()
        {
            IEnumerable<Order> userOrders;

            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            if (User.IsInRole("Admin"))
            {
                userOrders = dbContext.Orders.Include(x => x.OrderElems).Where(x => x.DateFinished == null).OrderByDescending(x => x.DatePurchased).ToList(); 
            }
            else
            {
                userOrders = dbContext.Orders.Include(x => x.OrderElems).Where(x => x.UserId == userId).OrderByDescending(x => x.DatePurchased).ToArray();
            }

            return View(userOrders);
        }
        [Authorize]
        public async Task<IActionResult> UserSettings()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            var user = await _userManager.Users.Include(x => x.UserData).SingleAsync(x => x.Id == userId);
            var userData = new UserSettingsViewModel()
            {
                UserSetting = user
            };

            return View(userData);
        }
        [HttpPost]
        public async Task<IActionResult> UserSettings(UserSettingsViewModel userData)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
                var user = await dbContext.Users.Include(x => x.UserData).FirstOrDefaultAsync(x => x.Id == userId);
                if (user == null) return RedirectToAction("Index", "Home");
                user.FirstName = userData.UserSetting.FirstName;
                user.LastName = userData.UserSetting.LastName;
                user.PhoneNumber = userData.UserSetting.PhoneNumber;
                user.UserData.Address = userData.UserSetting.UserData.Address;
                user.UserData.City = userData.UserSetting.UserData.City;
                user.UserData.CompanyName = userData.UserSetting.UserData.CompanyName;
                user.UserData.Nip = userData.UserSetting.UserData.Nip;
                user.UserData.ZipCode = userData.UserSetting.UserData.ZipCode;
                await dbContext.SaveChangesAsync();
            }
            else
            {
                return View("UserSettings", userData);
            }
            TempData["slideMsg"] = "Pomyślnie zaktualizowano dane użytkownika.";
            return View(userData);
        }
        public IActionResult ChangePassword()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(UserSettingsViewModel model)
        {

            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            var user = await _userManager.FindByIdAsync(userId);
            if (model.UserChangePassword.NewPassword != model.UserChangePassword.ConfirmNewPassword) return View("UserSettings");
            var changeUserPassword = await _userManager.ChangePasswordAsync(user, model.UserChangePassword.OldPassword, model.UserChangePassword.NewPassword);
            if (!changeUserPassword.Succeeded)
            {
                TempData["slideMsg"] = "Błąd podczas zmiany hasła.";
                return View("UserSettings", model);
            }
            TempData["slideMsg"] = "Twoje hasło zostało zmienione.";
            return View("UserSettings");
        }
        //[Authorize(Roles = "Admin")]
        //public IActionResult AdminUserOrders()
        //{
        //    if (User.IsInRole("Admin"))
        //    {
        //        IEnumerable<Order> showAllOrders;
        //        showAllOrders = dbContext.Orders.Include(x => x.OrderElems).OrderByDescending(x => x.Id).ToList(); // .Where(x => x.DateFinished == null)
        //        return View(showAllOrders);
        //    }
        //    return View();
        //}
        [Authorize(Roles = "Admin")]
        public OrderStatus ChangeOrderState(Order order)
        {
            Order orderToModify = dbContext.Orders.Find(order.Id);
            orderToModify.DateFinished = DateTime.Now;
            orderToModify.OrderStatus = order.OrderStatus;
            dbContext.SaveChanges();

            return order.OrderStatus;
        }
        [Authorize(Roles = "Admin")]
        public IActionResult AddProduct(int? productId)
        {
            Product product;
            IEnumerable<ProductsGallery> productsGallery;
            IEnumerable<Manufacturer> manufacturers;
            List<ProductsSpecValue> productSpecValues;
            IEnumerable<ProductsSpecification> productsSpecifications;
            if (productId > 0)
            {
                product = dbContext.Products.Include(x => x.Warehouse).Include(x => x.Manufacturer).Where(x => x.Id == productId).FirstOrDefault();
                productsGallery = dbContext.ProductsGalleries.Where(x => x.ProductId == productId);
                productSpecValues = dbContext.ProductsSpecValues.Include(x => x.Specification).Where(x => x.ProductId == productId).ToList();
                productsSpecifications = dbContext.ProductsSpecifications.Include(x => x.ProductsSpecValues).ToList();
            }
            else
            {
                product = new Product();
                productSpecValues = new List<ProductsSpecValue>();
                manufacturers = dbContext.Manufacturers.ToList();
                productsSpecifications = dbContext.ProductsSpecifications.Include(x => x.ProductsSpecValues).ToList();
            }
            var addProductsVM = new AddProductViewModel
            {
                Categories = dbContext.Categories.ToList(),
                Product = product,
                Manufacturers = dbContext.Manufacturers.ToList(),
                ProductsSpecifications = productsSpecifications
        };
            if (productId > 0) addProductsVM.ProductSpecValues = productSpecValues;
            return View(addProductsVM);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddProduct(AddProductViewModel model, List<IFormFile> DescriptionPhotos)
        {
            //var listOfProducts = dbContext.Products.ToList();
            //if (listOfProducts.Where(x => x.Name == model.Product.Name).Any()) return RedirectToAction("AddProduct", TempData["ProductMssg"] = "Taki produkt już istnieje w bazie danych.");
            if (model.Product.Id > 0)
            {
                if (ModelState.IsValid)
                {
                    model.Product.ProductTimeModified = DateTime.Now;
                    var specsToModify = dbContext.ProductsSpecValues.Where(x => x.ProductId == model.Product.Id).ToList();
                    foreach (var editSpecValues in specsToModify.Zip(model.ProductSpecValues, (a, b) => new { A = a, B = b }))
                    {
                        if (editSpecValues.B.SpecValue == null) continue;
                        editSpecValues.A.SpecValue = editSpecValues.B.SpecValue;
                        editSpecValues.A.SpecificationId = editSpecValues.B.SpecificationId;
                    }
                    var quantityToModify = dbContext.Warehouses.FirstOrDefault(x => x.ProductId == model.Product.Id);
                    quantityToModify.Quantity = model.Product.Warehouse.Quantity;
                    dbContext.Entry(model.Product.Warehouse).State = EntityState.Detached;
                    dbContext.Entry(model.Product).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    TempData["slideMsg"] = "Produkt został pomyślnie zaktualizowany.";
                    return RedirectToAction("AddProduct");
                }
            }
            if (model.ProductPhotosToUpload != null)
            {
                if (ModelState.IsValid)
                {
                    var productUniqueId = Guid.NewGuid().ToString();
                    var newProduct = new Product()
                    {
                        ManufacturerId = model.Product.ManufacturerId,
                        CategoryId = model.Product.CategoryId,
                        Warehouse = model.Product.Warehouse,
                        Name = model.Product.Name,
                        Model = model.Product.Model,
                        Price = model.Product.Price,
                        Description = model.Product.Description,
                        ProductUniqueId = productUniqueId,
                        ProductTimeCreated = DateTime.Now
                    };
                    dbContext.Products.Add(newProduct);

                    dbContext.SaveChanges();

                    var directory = $"img/Products/{productUniqueId}/";
                    var checkPath = $"wwwroot\\img\\Products\\{productUniqueId}";
                    if (!Directory.Exists(checkPath)) Directory.CreateDirectory(checkPath);

                    foreach (var formPhotos in model.ProductPhotosToUpload)
                    {
                        var productGalleryUniqueId = Guid.NewGuid();
                        var fileExt = Path.GetExtension(formPhotos.FileName);
                        var changeFileName = directory + productGalleryUniqueId + fileExt;

                        var filePath = Path.Combine(_webHostEnvironment.WebRootPath, changeFileName);
                        using (var stream = System.IO.File.Create(filePath))
                        {
                            await formPhotos.CopyToAsync(stream);
                        }
                        var pathToImg = "/" + changeFileName;
                        var newProductPhotos = new ProductsGallery
                        {
                            PhotoUrl = pathToImg,
                            ProductId = newProduct.Id
                        };
                        dbContext.ProductsGalleries.Add(newProductPhotos);
                    }
                    foreach (var specValues in model.ProductSpecValues)
                    {
                        if (specValues.SpecValue == null) continue;
                        var newProductSpecificationValues = new ProductsSpecValue
                        {
                            ProductId = newProduct.Id,
                            SpecificationId = specValues.SpecificationId,
                            SpecValue = specValues.SpecValue
                        };
                        dbContext.ProductsSpecValues.Add(newProductSpecificationValues);
                    }
                    dbContext.SaveChanges();
                    TempData["slideMsg"] = "Produkt został poprawnie dodany.";
                    return RedirectToAction("AddProduct");
                }
                else
                {
                    var categories = dbContext.Categories.ToList();
                    model.Categories = categories;
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("", "Nie wskazano pliku");
                var categories = dbContext.Categories.ToList();
                model.Categories = categories;
                return View(model);
            }
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddProductSpecification()
        {
            IEnumerable<ProductsSpecification> specs;
            specs = await dbContext.ProductsSpecifications.ToListAsync();
            return View(specs);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public JsonResult AddProductSpecification([FromBody] List<ProductsSpecification> listOfNewSpecifications)
        {
            var checkSpec = dbContext.ProductsSpecifications.ToList();
            foreach (var spec in listOfNewSpecifications)
            {
                if (checkSpec.Where(x => x.SpecName.Contains(spec.SpecName)).Any()) continue;
                var newSpec = new ProductsSpecification
                {
                    SpecName = spec.SpecName
                };
                dbContext.ProductsSpecifications.Add(newSpec);
            }
            dbContext.SaveChanges();
            return Json(new { Status = "success", responseText = "Pomyślnie dodałeś specyfikacje" });
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public JsonResult RemoveProductSpecification([FromBody] int specificationId)
        {
            var checkSpec = dbContext.ProductsSpecifications.ToList();
            var findSpecificationToRemove = checkSpec.Any(x => x.Id == specificationId);
            if (findSpecificationToRemove)
            {
                dbContext.ProductsSpecifications.Remove(checkSpec.FirstOrDefault(x => x.Id == specificationId));
                dbContext.SaveChanges();
                return Json(new { Status = "success", responseText = "Pomyślnie usunąłeś specyfikacje" });
            }

            return Json(new { Status = "error", responseText = "Błąd!" });
        }
        [Authorize(Roles = "Admin")]
        public IActionResult AddCategory()
        {
            IEnumerable<Category> categories;
            categories = dbContext.Categories.ToList();
            return View(categories);
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<JsonResult> AddCategory([FromBody] Category model)
        {
            var checkCategory = await dbContext.Categories.ToListAsync();
            if (checkCategory.Where(x => x.Name == model.Name).Any()) return Json(new { Status = "error", responseText = "Istnieje już taka kategoria w bazie danych" });
            var newCategory = new Category
            {
                Name = model.Name,
                CategoryIconName = model.CategoryIconName,
                ParentCategoryId = model.ParentCategoryId
            };
            await dbContext.Categories.AddAsync(newCategory);
            await dbContext.SaveChangesAsync();

            return Json(new { Status = "success", responseText = "Pomyślnie dodałeś kategorię" });
        }
        [Authorize(Roles = "Admin")]
        public JsonResult EditCategory([FromBody] Category model)
        {
            var checkCategory = dbContext.Categories.ToList();
            if (checkCategory.Where(x => x.Name == model.Name).Any()) return Json(new { Status = "error", responseText = "Istnieje już taka kategoria w bazie danych" });
            Category categoryToModify = dbContext.Categories.Find(model.Id);
            if (model.ParentCategoryId == 0) categoryToModify.ParentCategoryId = null; // do przemyślenia
            if (model.ParentCategoryId != 0) categoryToModify.ParentCategoryId = model.ParentCategoryId;
            if (model.Name != null) categoryToModify.Name = model.Name;
            if (model.CategoryIconName != null) categoryToModify.CategoryIconName = model.CategoryIconName;
            dbContext.SaveChanges();
            TempData["slideMsg"] = "Pomyślnie zedytowałeś kategorię";
            return Json(new { Status = "success", responseText = TempData["slideMsg"] });
        }
        [Authorize(Roles = "Admin")]
        public IActionResult AddManufacturer()
        {
            var manufacturers = dbContext.Manufacturers.ToList();
            var manufacturersVM = new AddManufacturerViewModel
            {
                ListOfManufacturers = manufacturers
            };
            return View(manufacturersVM);
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddManufacturer(AddManufacturerViewModel model)
        {
            var listOfManufacturers = await dbContext.Manufacturers.ToListAsync();
            if (listOfManufacturers.Where(x => x.Name == model.Manufacturer.Name).Any()) return RedirectToAction("AddManufacturer", TempData["ManufacturerMssg"] = "Taki producent już istnieje w bazie danych.");
            if (model.ManufacturerLogo != null)
            {
                if (ModelState.IsValid)
                {
                    IFormFile manufacturerLogo = model.ManufacturerLogo;
                    var fileExt = Path.GetExtension(manufacturerLogo.FileName);
                    var directory = "img/Manufacturers/";
                    var changeFileName = directory + model.Manufacturer.Name.ToLower() + fileExt;
                    var checkPath = "wwwroot\\img\\Manufacturers";
                    if (!Directory.Exists(checkPath)) Directory.CreateDirectory(checkPath);

                    var filePath = Path.Combine(_webHostEnvironment.WebRootPath, changeFileName);

                    await manufacturerLogo.CopyToAsync(new FileStream(filePath, FileMode.Create));

                    var pathToImg = "/" + changeFileName;
                    if (model.Manufacturer.Id != 0)
                    {
                        var checkIfLogoExist = listOfManufacturers.FirstOrDefault(x => x.Id == model.Manufacturer.Id).ManufacturerLogoPath;
                        if (System.IO.File.Exists(checkIfLogoExist)) System.IO.File.Delete(checkIfLogoExist);
                        var manufacturerToModify = dbContext.Manufacturers.Find(model.Manufacturer.Id);
                        manufacturerToModify.Name = model.Manufacturer.Name;

                        manufacturerToModify.ManufacturerLogoPath = pathToImg;
                        TempData["slideMsg"] = "Pomyślnie zmieniłeś dane producenta";
                    }
                    else
                    {
                        var newManufacturer = new Manufacturer
                        {
                            Name = model.Manufacturer.Name,
                            ManufacturerLogoPath = pathToImg
                        };
                        dbContext.Manufacturers.Add(newManufacturer);
                        TempData["slideMsg"] = "Pomyślnie dodałeś producenta";
                    }
                    dbContext.SaveChanges();
                }
                else
                {
                    var categories = dbContext.Manufacturers.ToList();
                    model.ListOfManufacturers = categories;
                    return View(model);
                }
            }
            return RedirectToAction("AddManufacturer");
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> HideProduct(AddProductViewModel model)
        {
            var findProductToHide = await dbContext.Products.FindAsync(model.Product.Id);
            findProductToHide.IsHidden = true;
            findProductToHide.ProductTimeModified = DateTime.Now;
            var newProductToHide = new HiddenProducts
            {
                ProductId = model.Product.Id,
                HideReason = model.HideProduct.HideReason,
                ProductHideDate = DateTime.Now
            };
            await dbContext.HiddenProducts.AddAsync(newProductToHide);
            await dbContext.SaveChangesAsync();
            TempData["slideMsg"] = "Pomyślnie ukryłeś produkt.";
            return RedirectToAction("AddProduct", new { model.Product.Id });
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UnhideProduct(int productId)
        {
            var findProductToUnhide = await dbContext.Products.FindAsync(productId);
            findProductToUnhide.IsHidden = false;
            findProductToUnhide.ProductTimeModified = DateTime.Now;
            await dbContext.SaveChangesAsync();
            TempData["slideMsg"] = "Pomyślnie przywróciłeś produkt.";
            return RedirectToAction("AddProduct");
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> HiddenProducts()
        {
            IEnumerable<Product> hiddenProducts;
            hiddenProducts = await dbContext.Products.Where(x => x.IsHidden).Include(x => x.ProductsGalleries).Include(x => x.HiddenProduct).OrderByDescending(x => x.ProductTimeModified).ToListAsync();

            /*var hideProducts = new AddProductViewModel();
            hideProducts.ProductsThatAreHide = findHideProducts;*/
            return View(hiddenProducts);
        }
    }
}
