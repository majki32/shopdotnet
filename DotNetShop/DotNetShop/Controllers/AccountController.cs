﻿using AutoMapper;
using DotNetShop.DAL.Model;
using DotNetShop.Interfaces;
using DotNetShop.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DotNetShop.Controllers
{
    public class AccountController : Controller
    {
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ILogger _logger;
        private readonly IMailSender _mailSender;
        public AccountController(IMapper mapper, UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager, SignInManager<AppUser> signInManager, ILogger<AccountController> logger, IMailSender mailSender)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _logger = logger;
            _mailSender = mailSender;
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.UserName == null) model.UserName = model.Email.Substring(0, model.Email.LastIndexOf("@"));

            var user = _mapper.Map<AppUser>(model);
            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.TryAddModelError(error.Code, error.Description);
                }
                return View(model);
            }
            await CreateUserData(user.Id);
            await _userManager.AddToRoleAsync(user, "User");

            string confirmationToken = _userManager.GenerateEmailConfirmationTokenAsync(user).Result;
            string confirmationLink = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token = confirmationToken }, Request.Scheme);

            _logger.LogInformation($"CE: {user.Email} confirmation email link: {confirmationLink}");
            
            var email = new Sender()
            {
                FirstName = user.FirstName,
                SendTo = user.Email,
                Subject = "Potwierdź adres email",
                EmailType = EmailEnums.ConfirmationLink
            };

            if (_mailSender.SendMail(email, confirmationLink))
            {
                TempData["slideMsg"] = "Rejestracja przebiegła pomyślnie, na podany adres e-mail został wysłany link aktywacyjny.";
            }
            else
            {
                TempData["slideMsg"] = "Błąd podczas wysyłania wiadomości";
            }

            return RedirectToAction("Index", "Home");
        }
        public IActionResult Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid) return View(model);

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                if (_userManager.IsEmailConfirmedAsync(user).Result)
                {

                    var identity = new ClaimsIdentity(IdentityConstants.ApplicationScheme);
                    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
                    identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
                    identity.AddClaim(new Claim("FirstName", user.FirstName));
                    identity.AddClaim(new Claim("LastName", user.LastName));
                    var roles = await _userManager.GetRolesAsync(user);
                    foreach (var role in roles)
                    {
                        identity.AddClaim(new Claim(ClaimTypes.Role, role));
                    }
                    await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, new ClaimsPrincipal(identity));

                    return RedirectToPreviousUrl(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Adres email nie został zweryfikowany!");
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("", "Niepoprawna nazwa użytkownika lub hasło");
                return View();
            }
        }
        public IActionResult ConfirmEmail(string userId, string token)
        {
            AppUser user = _userManager.FindByIdAsync(userId).Result;
            IdentityResult result = _userManager.ConfirmEmailAsync(user, token).Result;
            if (result.Succeeded)
            {
                TempData["slideMsg"] = "Adres email został poprawnie zweryfikowany, możesz teraz się zalogować.";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["slideMsg"] = "Błąd podczas weryfikacji adresu email.";
                return RedirectToAction("Index", "Home");
            }
        }
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Index", "Home");
        }
        private IActionResult RedirectToPreviousUrl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            else
                return RedirectToAction("Index", "Home");
        }
        private async Task CreateUserData(string userId)
        {
            using (var dbContext = new MicomDBContext())
            {
                var userData = await dbContext.UserDatas.FirstOrDefaultAsync(x => x.UserId == userId);

                if (userData == null)
                {
                    var data = new UserData()
                    {
                        UserId = userId
                    };
                    await dbContext.UserDatas.AddAsync(data);
                }
                await dbContext.SaveChangesAsync();
            }
        }
        //GET
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null && await _userManager.IsEmailConfirmedAsync(user))
                {
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var passwordResetLink = Url.Action("ResetPassword", "Account", new { userId = user.Id, token = token }, Request.Scheme);

                    _logger.LogInformation($"RP: {user.Email} reset password link: {passwordResetLink}");

                    var email = new Sender()
                    {
                        FirstName = user.FirstName,
                        SendTo = user.Email,
                        Subject = "Resetowanie hasła",
                        EmailType = EmailEnums.PasswordReset
                    };

                    if (_mailSender.SendMail(email, passwordResetLink))/*SendMail(email, passwordResetLink)*/
                    {
                        TempData["slideMsg"] = $"Wiadomość z przypomnieniem hasła została poprawnie wysłana na adres {user.Email}.";
                    }
                    else
                    {
                        TempData["slideMsg"] = "Błąd podczas wysyłania wiadomości";
                    }


                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Taki użytkownik nie istnieje lub adres e-mail nie został zweryfikowany.");
                }
            }
            return View();
        }
        //GET
        public IActionResult ResetPassword(string userId, string token)
        {
            if (token == null || userId == null)
            {
                ModelState.AddModelError("", "Niepoprawny token");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.UserId);
                if (user != null)
                {
                    var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        TempData["slideMsg"] = "Hasło zostało zresetowane!";
                        return RedirectToAction("Index", "Home");
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.TryAddModelError("", error.Description);
                    }
                    return View(model);
                }
            }
            return View(model);
        }
    }
}
