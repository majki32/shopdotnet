﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetShop.DAL.Model;
using DotNetShop.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace DotNetShop.Controllers
{
    public class StoreController : Controller
    {
        private IMemoryCache _cache;
        private readonly MicomDBContext dbContext = new MicomDBContext();

        public StoreController(IMemoryCache cache)
        {
            _cache = cache;
        }

        public IActionResult List(int categoryId)
        {
            var listCategoryId = dbContext.Categories.FirstOrDefault(b => b.Id == categoryId).Id;
            var popProd = dbContext.Products.Where(c => c.CategoryId == listCategoryId && c.IsHidden == false).OrderBy(g => Guid.NewGuid()).Take(3).ToList();
            var prod = dbContext.Products.Include(x => x.ProductsGalleries).Include(x => x.ProductsSpecValues).ThenInclude(x => x.Specification).Where(c => c.CategoryId == listCategoryId).ToList();
            var listVM = new StoreViewModel()
            {
                Products = prod,
                PopularProducts = popProd
            };

            return View(listVM);
        }
        public IActionResult Details(string id)
        {
            var productDetail = dbContext.Products.Include(x => x.ProductsGalleries)
                .Include(x => x.ProductsSpecValues)
                .ThenInclude(x => x.Specification)
                .Include(x => x.Manufacturer)
                .Include(x => x.Warehouse)
                .Where(p => p.Id == int.Parse(id))
                .FirstOrDefault();
            bool isAdmin = User.IsInRole("Admin");
            ViewBag.UserIsAdmin = isAdmin;

            return View(productDetail);
        }

        public IActionResult AllCategories(int mainCategoryId)
        {
            var actualMainCategoryId = dbContext.Categories.FirstOrDefault(b => b.Id == mainCategoryId).Id;
            var actualMainSubCategoryId = dbContext.Categories.Where(sc => sc.ParentCategoryId == actualMainCategoryId).FirstOrDefault().Id;
            var actualMainCategory = dbContext.Categories.Where(x => x.Id == actualMainCategoryId).ToList();
            var actualMainSubCategory = dbContext.Categories.Where(c => c.ParentCategoryId == actualMainCategoryId).ToList();
            var actualMainSubSubCategory = dbContext.Categories.Where(d => d.ParentCategoryId == actualMainSubCategoryId).ToList();
            var countProductOnSubCategory = dbContext.Categories.Include(prods => prods.Products).Count();
            var countSubCategories = dbContext.Categories.Where(d => d.ParentCategoryId == actualMainSubCategoryId).Count();
            var cate = new CategoriesViewModel()
            {
                ActualCategory = actualMainCategory,
                ActualParentCategory = actualMainSubCategory,
                ActualParentSubCategory = actualMainSubSubCategory,
                CountProductOnSubCategory = countProductOnSubCategory,
                CountSubCategories = countSubCategories
            };
            return View(cate);
        }
        public IActionResult ProductsSuggestions(string term)
        {
            var products = this.dbContext.Products
                .Where(p => p.Name.ToLower()
                .Contains(term.ToLower()))
                .Take(5)
                .Select(p => new
                {
                    label = p.Name,
                    photo = p.ProductsGalleries.Select(photoUrl => photoUrl.PhotoUrl).Take(1)
                });
            return new JsonResult(products);
        }
        public IActionResult SearchResult()
        {
            var queryTerm = (string)default;
            if (!(Request.HasFormContentType && Request.Form != null && Request.Form.Count() > 0)) return RedirectToAction("Cart", "Cart");
            else
            {
                queryTerm = Request.Form["topSearchInputQuery"].ToString();
            }
            if (dbContext.Products.Any(cp => cp.Name == queryTerm)) return RedirectToAction("Details", new { id = dbContext.Products.FirstOrDefault(cp => cp.Name == queryTerm).Id });

            var qProducts = dbContext.Products.Include(ph => ph.ProductsGalleries).Include(x => x.ProductsSpecValues).ThenInclude(x => x.Specification).Where(p => p.Name.ToLower().Contains(queryTerm.ToLower())).ToList();


            var queryProducts = new StoreViewModel()
            {
                Products = qProducts,
                QueryTerm = queryTerm
            };


            return View(queryProducts);
        }
    }
}
