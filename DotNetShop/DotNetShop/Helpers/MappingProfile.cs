﻿using AutoMapper;
using DotNetShop.DAL.Model;
using DotNetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RegisterViewModel, AppUser>().ForMember(u => u.UserName, options => options.MapFrom(x => x.UserName));
            CreateMap<RegisterViewModel, AppUser>().ForMember(u => u.FirstName, options => options.MapFrom(x => x.FirstName));
            CreateMap<RegisterViewModel, AppUser>().ForMember(u => u.LastName, options => options.MapFrom(x => x.LastName));
        }
    }
}
