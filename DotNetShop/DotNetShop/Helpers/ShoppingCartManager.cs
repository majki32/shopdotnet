﻿using DotNetShop.DAL.Model;
using DotNetShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotNetShop.Helpers
{
    public class ShoppingCartManager
    {
        private ISession _session;

        private MicomDBContext dbContext = new MicomDBContext();
        private List<CartItem> cart = new List<CartItem>();
        public ShoppingCartManager(ISession session)
        {
            _session = session;
            cart = _session.GetCart();
        }

        public void AddToCart(int productId)
        {

            var cartItem = cart.Find(c => c.Product.Id == productId);

            if (cartItem != null)
            {
                if (cartItem.Quantity < 2)
                    cartItem.Quantity++;
            }
            else
            {
                var productToAdd = dbContext.Products.Include(x => x.Warehouse).Include(x => x.ProductsSpecValues).ThenInclude(x => x.Specification).Where(a => a.Id == productId).SingleOrDefault();
                if (productToAdd != null)
                {
                    var newCartItem = new CartItem()
                    {
                        Product = productToAdd,
                        Quantity = 1,
                        TotalPrice = productToAdd.Price

                    };
                    cart.Add(newCartItem);
                }
            }
            _session.SetObjectAsJson(SessionExtensions.CartSessionKey, cart);
        }
        public int RemoveFromCart(int productId)
        {

            var cartItem = cart.Find(c => c.Product.Id == productId);

            if (cartItem != null)
            {
                if (cartItem.Quantity > 1)
                {
                    cartItem.Quantity--;
                    _session.SetObjectAsJson(SessionExtensions.CartSessionKey, cart);
                    return cartItem.Quantity;
                }
                else
                {
                    cart.Remove(cartItem);
                    _session.SetObjectAsJson(SessionExtensions.CartSessionKey, cart);
                }
            }
            return 0;
        }
        public decimal GetCartTotalPrice()
        {
            return cart.Sum(sum => (sum.Quantity * sum.Product.Price));
        }
        public int GetCartItemsCount()
        {
            int countCartItems = cart.Sum(c => c.Quantity);

            return countCartItems;
        }
        public decimal GetCartItemPrice(int itemCount, int productId)
        {
            decimal cartItemPrice = cart.Where(x => x.Product.Id == productId).Sum(x => itemCount * x.Product.Price);

            return cartItemPrice;
        }
        public Order CreateOrder(Order newOrder, string userId)
        {
            newOrder.DatePurchased = DateTime.Now;
            newOrder.UserId = userId;

            dbContext.Orders.Add(newOrder);

            if(newOrder.OrderElems == null) newOrder.OrderElems = new List<OrderElem>();

            List<Warehouse> modifyQuantity = new List<Warehouse>();
            decimal cartTotal = 0;
            decimal elemsPrice = 0;

            foreach (var cartItem in cart)
            {
                elemsPrice += (cartItem.Quantity * cartItem.Product.Price);
                var newOrderItem = new OrderElem()
                {
                    ProductId = cartItem.Product.Id,
                    Quantity = cartItem.Quantity,
                    UnitPrice = elemsPrice
                };
                cartTotal += (cartItem.Quantity * cartItem.Product.Price);
                newOrder.OrderElems.Add(newOrderItem);
                var modifyProductQuantity = dbContext.Warehouses.FirstOrDefault(x => x.ProductId == cartItem.Product.Id);
                modifyProductQuantity.Quantity -= cartItem.Quantity;
                modifyQuantity.Add(modifyProductQuantity);
                elemsPrice = 0;
            }
            newOrder.TotalPrice = cartTotal;
            dbContext.SaveChanges();

            _session.EmptyCart(cart);

            return newOrder;
        }
    }
}
