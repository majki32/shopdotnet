﻿using DotNetShop.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class Sender : IMailSender
    {
        private readonly ILogger _logger;
        public Sender()
        {

        }
        public Sender(ILogger logger)
        {
            _logger = logger;
        }
        public string FirstName { get; set; }
        public string SendTo { get; set; }
        public string Subject { get; set; }
        public EmailEnums EmailType { get; set; }

        public bool SendMail(Sender emailAddress, string link)
        {
            string mailFromUser = "micomkontakt@gmail.com";
            string mailFromPassword = "1234567q^";

            MailAddress SendFrom = new MailAddress(mailFromUser);
            MailAddress SendTo = new MailAddress(emailAddress.SendTo);

            using (var message = new MailMessage(SendFrom, SendTo))
            {
                message.IsBodyHtml = true;
                message.Subject = emailAddress.Subject;
                AlternateView htmlView = null;

                var body = "<!DOCTYPE HTML>";
                body += "<html><head><meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">";
                body += $"<style></style></head><body><div><p>Witaj, {emailAddress.FirstName}.</p></div>";
                if (emailAddress.EmailType == EmailEnums.ConfirmationLink)
                {
                    body += "<div><p>Dziękujemy za rejestrajcę w naszym serwisie, poniżej znajduje się link aktywacyjny.</p>";
                    body += "<p style=>Jeśli to nie Ty rejestrowałeś się za pomocą tego maila, zignoruj tą wiadomość.</p>";
                    body += $"<div style='display: flex; align-items: center;'><a href='{link}' style='display: flex; align-items: center; padding: 12px;margin: 0 auto;border: 0;border-radius: 6px;background-color: #4B8BF4;color: white;font-size: 16px;text-align: center;text-decoration: none; '>Potwierdź adres email</a></div>";
                }
                else if (emailAddress.EmailType == EmailEnums.PasswordReset)
                {
                    body += "<div><p>Bardzo nam przykro, że nie byłeś w stanie zapamiętać swojego hasła do naszego serwisu :(</p>";
                    body += "<p>Poniżej znajduje się link do zresetowania Twojego hasła.</p>";
                    body += "<p>Jeśli to nie Ty wysłałeś prośbę o zresetowanie hasła, zignoruj tą wiadomość.</p>";
                    body += $"<div style='display: flex; justify-content: center; align-items: center;'><a href='{link}' style='padding: 12px;margin: 0 auto;border: 0;border-radius: 6px;background-color: #4B8BF4;color: white;font - size: 16px;text-align: center;text-decoration: none; '>Zresetuj hasło</a></div>";
                }
                else if(emailAddress.EmailType == EmailEnums.NewOrder)
                {

                }
                body += "</div></body></html>";

                htmlView = AlternateView.CreateAlternateViewFromString(body, new ContentType(MediaTypeNames.Text.Html));
                htmlView.ContentType.CharSet = Encoding.UTF8.WebName;
                message.AlternateViews.Add(htmlView);

                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Host = "smtp.gmail.com";
                    smtpClient.EnableSsl = true;
                    NetworkCredential cred = new NetworkCredential(mailFromUser, mailFromPassword);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = cred;
                    smtpClient.Port = 587;
                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"SM: {ex.Message}");
                        return false;
                    }
                    return true;
                }
            }
        }
    }
    public enum EmailEnums
    {
        ConfirmationLink = 0,
        PasswordReset = 1,
        Contact = 2,
        NewOrder = 3
    }
}
