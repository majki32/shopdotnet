﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class UserSettingsViewModel
    {
        public AppUser UserSetting { get; set; }
        public ChangePasswordViewModel UserChangePassword { get; set; }
    }
}
