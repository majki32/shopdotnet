﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class CategoriesViewModel
    {
        public IEnumerable<Category> ActualCategory { get; set; }
        public IEnumerable<Category> ActualParentCategory { get; set; }
        public IEnumerable<Category> ActualParentSubCategory { get; set; }
        public int CountProductOnSubCategory { get; set; }
        public int CountSubCategories { get; set; }
    }
}
