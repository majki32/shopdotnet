﻿using DotNetShop.DAL.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class AddProductViewModel
    {
        public Product Product { get; set; }
        public List<IFormFile> ProductPhotosToUpload { get; set; }
        public List<ProductsSpecValue> ProductSpecValues { get; set; }
        public HiddenProducts HideProduct { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Manufacturer> Manufacturers { get; set; }
        public IEnumerable<ProductsSpecification> ProductsSpecifications { get; set; }
    }
}
