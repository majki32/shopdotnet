﻿using System.ComponentModel.DataAnnotations;

namespace DotNetShop.Models
{
    public class LoginViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podanie adresu e-mail jest wymagane")]
        [EmailAddress(ErrorMessage = "Wprowadź poprawny adres e-mail")]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Wprowadź hasło")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Podanie adresu e-mail jest wymagane")]
        [EmailAddress(ErrorMessage = "Wprowadź poprawny adres e-mail")]
        public string Email { get; set; }
        public string UserName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Wprowadź swoje hasło")]
        [StringLength(50, ErrorMessage = "Hasło może zawierać maksymalnie 50 znaków"), MinLength(6, ErrorMessage = "Hasło musi zawierać minimum 6 znaków")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Podane przez Ciebie hasła różnią się")]
        public string ConfirmPassword { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj swoje imię")]
        public string FirstName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Podaj swoje nazwisko")]
        public string LastName { get; set; }
        [Required]
        [Compare("isTrue", ErrorMessage = "Musisz potwierdzić regulamin")]
        public bool ConfirmTerms { get; set; }
        public bool isTrue { get { return true; } }
    }
    public class ChangePasswordViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Wymagane podanie starego hasła")]
        [DataType(DataType.Password)] 
        public string OldPassword { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Wymagane podanie nowego hasła")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Podane przez Ciebie hasła różnią się")]
        [DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }
    }
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Podanie adresu e-mail jest wymagane")]
        [EmailAddress(ErrorMessage = "Wprowadź poprawny adres e-mail")]
        public string Email { get; set; }
    }
    public class ResetPasswordViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Podane przez Ciebie hasła różnią się")]
        public string ConfirmPassword { get; set; }
        public string Token { get; set; }
    }
}
