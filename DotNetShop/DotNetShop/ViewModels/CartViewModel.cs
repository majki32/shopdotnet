﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class CartViewModel
    {
        public List<CartItem> CartItems { get; set; }
        public IEnumerable<ProductsGallery> ProductPhoto { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
