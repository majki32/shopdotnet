﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class StoreViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<ProductsGallery> ProductsPhotos { get; set; }
        public IEnumerable<Product> PopularProducts { get; set; }
        public IEnumerable<Category> ActualCategory { get; set; }
        public string QueryTerm { get; set; }
    }
}
