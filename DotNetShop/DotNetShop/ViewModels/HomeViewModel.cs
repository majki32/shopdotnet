﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class HomeViewModel
    {
        public IQueryable<Product> Products { get; set; }
        public IList<Product> TopProductBuys { get; set; }
        public IList<Product> LastProductBuys { get; set; }
    }
}
