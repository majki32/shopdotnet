﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class ProductViewModel
    {
        public Product Product { get; set; }
        public IEnumerable<ProductsGallery> ProductGallery { get; set; }
        public IEnumerable<ProductsSpecValue> ProductSpecification { get; set; }
    }
}
