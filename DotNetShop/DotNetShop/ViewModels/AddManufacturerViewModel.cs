﻿using DotNetShop.DAL.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class AddManufacturerViewModel
    {
        public Manufacturer Manufacturer { get; set; }
        public IFormFile ManufacturerLogo { get; set; }
        public IEnumerable<Manufacturer> ListOfManufacturers { get; set; }
    }
}
