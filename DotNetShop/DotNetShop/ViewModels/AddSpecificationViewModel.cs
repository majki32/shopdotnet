﻿using DotNetShop.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Models
{
    public class AddSpecificationViewModel
    {
        public IList<ProductsSpecification> ProductSpecs { get; set; }
        public IList<ProductsSpecValue> ProductsSpecsValue { get; set; }
    }
}
