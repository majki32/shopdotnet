﻿using DotNetShop.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Helpers
{
    public static class SessionExtensions
    {
        public const string CartSessionKey = "CartData";
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
        public static List<CartItem> GetCart(this ISession session)
        {
            return session.GetObjectFromJson<List<CartItem>>(CartSessionKey) ?? new List<CartItem>();
        }
        public static void SaveCart(this ISession session, List<CartItem> cart)
        {
            session.SetObjectAsJson(CartSessionKey, cart);
        }
        public static void EmptyCart(this ISession session, List<CartItem> cart)
        {
            cart = null;
            session.SetObjectAsJson(CartSessionKey, cart);
        }
    }
}
