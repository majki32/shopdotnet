﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(function () {
    $(".quickBasketClose").click(function () {
        $("#quickBasketContainer").css("display", "none");
    });
    $("#quickBasket").click(function () {
        $("#quickBasketContainer").css("display", "flex");
    });
});
$(function () {

    var setupAutoComplete = function () {
        var $input = $(this);

        var options = {
            source: $input.attr("data-query-source"),
            select: function (event, ui) {
                $inputName = $(this);
                $inputName.val(ui.item.label);
                var $form = $input.parents("#topSearchBarForm");
                $form.submit();
            }
        };
        $input.autocomplete(options).data("ui-autocomplete")._renderItem = function (ul, item) {
            var showListItems = '<a><div class="searchTopBarListItemsContainer"><img src="' + item.photo + '" class="topSearchBarListItemImg"><div class="topSearchBarItemListName"> ' + item.label + '</div></div></a>';
            return $("<li></li>")
                .data("ui-autocomplete-item", item)
                .append(showListItems)
                .appendTo(ul);
        };
    };
    $("#topSearchBarInput").each(setupAutoComplete);
});
$(".mobSearch").click(function () {
    if ($(".search-box").css("display") == 'none') {
        $(".search-box").css("display", "flex");
    } else {
        $(".search-box").css("display", "none");
    }
});
$(".catMobMenu").click(function () {
    if ($(".nav-container").css("display") == 'none') {
        $(".nav-container").css("display", "flex");
    } else {
        $(".nav-container").css("display", "none");
    }
});
$(".slideMessageCloseBttn").click(function () {
    $('.slideMessage').hide();
});
