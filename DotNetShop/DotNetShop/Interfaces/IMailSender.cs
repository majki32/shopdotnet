﻿using DotNetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetShop.Interfaces
{
    public interface IMailSender
    {
       bool SendMail(Sender email, string link = null);
    }
}
